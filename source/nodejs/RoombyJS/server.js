var gcm = require('node-gcm');

//var message = new gcm.Message();

var message = new gcm.Message({
    collapseKey: 'demo',
    delayWhileIdle: true,
    timeToLive: 4,
    data: {
        key1: 'FirstMessage',
        key2: 'SecondMessage'
    }
});

var sender = new gcm.Sender('AIzaSyDklZdTzLXsYUiHpIBuzKYuK-wvjB1alDw');
var registrationIds = [];

registrationIds.push('regId1');

sender.send(message, registrationIds, 4, function( err, result)
{
	console.log(result);
});