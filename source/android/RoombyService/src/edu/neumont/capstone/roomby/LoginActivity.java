package edu.neumont.capstone.roomby;

import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Image;

/**
 * 
 * @author Daniel Wood
 *
 */
public class LoginActivity extends ActionBarActivity
							implements GooglePlayServicesClient.OnConnectionFailedListener, GooglePlayServicesClient.ConnectionCallbacks, ConnectionCallbacks, OnConnectionFailedListener{

	/* Request code used to invoke sign in user interactions. */
	  private static final int RC_SIGN_IN = 0;

	  /* Client used to interact with Google APIs. */
	  private GoogleApiClient mGoogleApiClient;

	  /* A flag indicating that a PendingIntent is in progress and prevents
	   * us from starting further intents.
	   */
	  private boolean mIntentInProgress;
	  private boolean mSignInClicked;
	  private ConnectionResult mConnectionResult;
	  private String currentUsername;
	  private TextView welcomeMessage;
	  private Person currentPerson;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(Plus.API)
        .addScope(Plus.SCOPE_PLUS_LOGIN)
        .addScope(Plus.SCOPE_PLUS_PROFILE)
        .build();
		
		setContentView(R.layout.activity_login);
		
		
		currentUsername = "nobody";
		welcomeMessage = (TextView) findViewById(R.id.welcomeText);
		SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
		Button signOutButton = (Button) findViewById(R.id.sign_out_button);
		
		signInButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if (view.getId() == R.id.sign_in_button
					    && !mGoogleApiClient.isConnecting()) {
					    mSignInClicked = true;
					    resolveSignInError();
					  }
//				welcomeMessage.setText(R.string.welcome_user + getSignedInUserName(mGoogleApiClient));
			}
		});
		signOutButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if (view.getId() == R.id.sign_out_button)
				{
					if (mGoogleApiClient.isConnected()) 
						{
					      Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
					      mGoogleApiClient.disconnect();
					      mGoogleApiClient.connect();
						}
					showToast("Signed out...", true);
					
					welcomeMessage.setText(getResources().getString(R.string.welcome_guest));
//					showToast(getSignedInUserName(mGoogleApiClient)+ " is still signed in...", false);
				}
			}
		});
		
//		if (savedInstanceState == null) {
//			getSupportFragmentManager().beginTransaction()
//					.add(R.id.container, new PlaceholderFragment()).commit();
//		}
	}

	private String getSignedInUserName(GoogleApiClient googleApi)
	{
		try {
			currentUsername = Plus.AccountApi.getAccountName(googleApi);
		} catch (Exception ex){
			currentUsername = "nobody";
		}
		return currentUsername;
	}

	private void showToast(String message, boolean isDisplayLengthLong)
	{
		if (isDisplayLengthLong)
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		else
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	private void resolveSignInError() {
		  if (mConnectionResult.hasResolution()) {
		    try {
		      mIntentInProgress = true;
		      mConnectionResult.startResolutionForResult(this,
                      RC_SIGN_IN);
		    } catch (SendIntentException e) {
		      // The intent was canceled before it was sent.  Return to the default
		      // state and attempt to connect to get an updated ConnectionResult.
		      mIntentInProgress = false;
		      mGoogleApiClient.connect();
		    }
		  }
	}
	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		if (requestCode == RC_SIGN_IN) {
		    if (responseCode != RESULT_OK) {
		      mSignInClicked = false;
		    }

		    mIntentInProgress = false;

		    if (!mGoogleApiClient.isConnecting()) {
		      mGoogleApiClient.connect();
		    }
		  }
		if (responseCode == RESULT_OK)
		{
			Intent mainIntent = new Intent(this, MainActivity.class);
			startActivity(mainIntent);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	protected void onStart() {
	    super.onStart();
	    mGoogleApiClient.connect();
	}

	@Override
	protected void onStop() {
	    super.onStop();

	    if (mGoogleApiClient.isConnected()) {
	      mGoogleApiClient.disconnect();
	    }
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress) {
		    // Store the ConnectionResult so that we can use it later when the user clicks
		    // 'sign-in'.
		    mConnectionResult = result;

		    if (mSignInClicked) {
		      // The user has already clicked 'sign-in' so we attempt to resolve all
		      // errors until the user is signed in, or they cancel.
		      resolveSignInError();
		    }
		  }
	}

	@Override
	public void onConnected(Bundle arg0) {
		mSignInClicked = false;
		currentUsername = Plus.AccountApi.getAccountName(mGoogleApiClient);
//		if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
		    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		    currentUsername = currentPerson.getDisplayName();
//		  }
		welcomeMessage.setText(getResources().getString(R.string.welcome_user)+ " " + currentUsername.split(" ")[0]);
		
	}

	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId())
		{
		case R.id.action_dashboard:
			Intent dashboardIntent = new Intent(this, MainActivity.class);
			startActivity(dashboardIntent);
			return true;
		case R.id.action_signout:
			if(mGoogleApiClient.isConnected())
			{
				if (mGoogleApiClient.isConnected()) 
				{
			      Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			      mGoogleApiClient.disconnect();
			      mGoogleApiClient.connect();
				}
			}
			showToast("Signed out...", true);
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			return true;
		case R.id.action_settings:
			return true;
		default: return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		welcomeMessage.setText(getResources().getString(R.string.welcome_guest));
		
	}
}
