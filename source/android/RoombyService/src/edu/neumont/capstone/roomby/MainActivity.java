package edu.neumont.capstone.roomby;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import edu.neumont.capstone.roomby.gdx.android.AndroidLauncher;

//import edu.neumont.capstone.roomby.android.RoombyGDXLauncher;

public class MainActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener, 
													com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, 
													com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {

	private static final int RC_SIGN_IN = 0;
	private GoogleApiClient mGoogleApiClient;
	
	private boolean mIntentInProgress;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	private TextView welcomeMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(Plus.API)
        .addScope(Plus.SCOPE_PLUS_LOGIN)
        .addScope(Plus.SCOPE_PLUS_PROFILE)
        .build();
		
		setContentView(R.layout.activity_main);
		
	
		welcomeMessage = (TextView) findViewById(R.id.main_welcome);
		
		Button launchBtn = (Button) findViewById(R.id.launchGDXbtn);
		launchBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent gdx_intent = new Intent(v.getContext(), AndroidLauncher.class);
				startActivity(gdx_intent);
				
			}});
	}
	
	

	@Override
	protected void onStart(){
		super.onStart();
	    mGoogleApiClient.connect();
	}
	
	@Override
	protected void onStop(){
		super.onStop();

	    if (mGoogleApiClient.isConnected()) {
	      mGoogleApiClient.disconnect();
	    }
	}
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress) {
		    // Store the ConnectionResult so that we can use it later when the user clicks
		    // 'sign-in'.
		    mConnectionResult = result;

		    if (mSignInClicked) {
		      // The user has already clicked 'sign-in' so we attempt to resolve all
		      // errors until the user is signed in, or they cancel.
		      resolveSignInError();
		    }
		  }
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		mSignInClicked = false;
		welcomeMessage = (TextView) findViewById(R.id.main_welcome);
		Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		welcomeMessage.setText(getResources().getString(R.string.welcome_user) + " " + 
		currentPerson.getDisplayName().split(" ")[0]);
		
	}

	@Override
	public void onDisconnected() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}
	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		if (requestCode == RC_SIGN_IN) {
		    if (responseCode != RESULT_OK) {
		      mSignInClicked = false;
		    }

		    mIntentInProgress = false;

		    if (!mGoogleApiClient.isConnecting()) {
		      mGoogleApiClient.connect();
		    }
		  }
	}
	private void resolveSignInError() {
		  if (mConnectionResult.hasResolution()) {
		    try {
		      mIntentInProgress = true;
		      mConnectionResult.startResolutionForResult(this,
                    RC_SIGN_IN);
		    } catch (SendIntentException e) {
		      // The intent was canceled before it was sent.  Return to the default
		      // state and attempt to connect to get an updated ConnectionResult.
		      mIntentInProgress = false;
		      mGoogleApiClient.connect();
		    }
		  }
	}
	private void showToast(String message, boolean isDisplayLengthLong)
	{
		if (isDisplayLengthLong)
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		else
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId())
		{
		case R.id.action_dashboard:
//			Intent dashboardIntent = new Intent(this, MainActivity.class);
//			startActivity(dashboardIntent);
			return true;
		case R.id.action_signout:
			if(mGoogleApiClient.isConnected())
			{
				if (mGoogleApiClient.isConnected()) 
				{
			      Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			      mGoogleApiClient.disconnect();
			      mGoogleApiClient.connect();
				}
			}
			showToast("Signed out...", true);
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			return true;
		case R.id.action_settings:
			return true;
		default: return super.onOptionsItemSelected(item);
		}
	}
}
