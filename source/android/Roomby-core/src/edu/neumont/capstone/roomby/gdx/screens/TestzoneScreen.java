/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActor;

public class TestzoneScreen implements com.badlogic.gdx.Screen {

	RoombyGDX game;
	Stage stage;
	
	public TestzoneScreen(RoombyGDX game){
		this.stage = new Stage();
		this.game = game;
	}
	public TestzoneScreen(RoombyGDX game, Stage stage){
		this.game = game;
		this.stage = stage;
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.49f, 1f, 0.74f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
		if(Gdx.input.isKeyPressed(Keys.BACK))
		{
			game.setScreen(game.dashboardScreen);
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void show() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
		
		final Skin skin = new Skin();
		skin.add("default", new LabelStyle(new BitmapFont(), Color.WHITE));
		skin.add("badlogic", new Texture("badlogic.jpg"));

		final Image sourceImage = new Image(skin, "badlogic");
		sourceImage.setBounds(250, 325, 200, 200);
		stage.addActor(sourceImage);

		final Image validTargetImage = new Image(skin, "badlogic");
		validTargetImage.setBounds(500, 250, 200, 200);
		validTargetImage.setColor(Color.GREEN);
		stage.addActor(validTargetImage);

		final Image invalidTargetImage = new Image(skin, "badlogic");
		invalidTargetImage.setBounds(500, 500, 200, 200);
		invalidTargetImage.setColor(Color.RED);
		stage.addActor(invalidTargetImage);

		DragAndDrop dragAndDrop = new DragAndDrop();
		dragAndDrop.addSource(new Source(sourceImage) {
			@Override
			public Payload dragStart (InputEvent event, float x, float y, int pointer) {
				Payload payload = new Payload();
				payload.setObject(sourceImage);

				payload.setDragActor(sourceImage);

				
				payload.setValidDragActor(validTargetImage);

				Label invalidLabel = new Label("Some payload!", skin);
				invalidLabel.setColor(1, 0, 0, 1);
				payload.setInvalidDragActor(invalidLabel);

				return payload;
			}
			@Override
			public void dragStop (InputEvent event, float x, float y, int pointer, Payload payload, Target target)
			{
				Actor temp = payload.getDragActor();
				temp.setColor(Color.WHITE);
				temp.setPosition(x+temp.getX(), y+temp.getY());
				stage.addActor(temp);
			}
		});
//		dragAndDrop.addTarget(new Target(validTargetImage) {
//			public boolean drag (Source source, Payload payload, float x, float y, int pointer) {
//				getActor().setColor(Color.GREEN);
//				return true;
//			}
//
//			public void reset (Source source, Payload payload) {
////				getActor().setScale(1/3);
//			}
//
//			public void drop (Source source, Payload payload, float x, float y, int pointer) {
////				System.out.println("Accepted: " + payload.getObject() + " " + x + ", " + y);
////				getActor().setScale(1/3);
//				MyActor copy = Serializer.copy(getActor(), MyActor.class);
//				copy.setBounds(x, y, copy.getWidth(), copy.getHeight());
//				stage.addActor(copy);
//			}
//		});
//		dragAndDrop.addTarget(new Target(invalidTargetImage) {
//			public boolean drag (Source source, Payload payload, float x, float y, int pointer) {
//				getActor().setColor(Color.RED);
//				return false;
//			}
//
//			public void reset (Source source, Payload payload) {
//				getActor().setColor(Color.WHITE);
//			}
//
//			public void drop (Source source, Payload payload, float x, float y, int pointer) {
//			}
//		});
//		
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.clear();
		stage.dispose();
		stage = null;
	}

}
