/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import java.util.Comparator;

import com.badlogic.gdx.scenes.scene2d.Actor;


public class ActorComparator implements Comparator < Actor > {
    @Override
    public int compare(Actor arg0, Actor arg1) {
        if (arg0.getZIndex() < arg1.getZIndex()) {
            return -1;
        } else if (arg0.getZIndex() == arg1.getZIndex()) {
            return 0;
        } else {
            return 1;
        }
    }
}
