package edu.neumont.capstone.roomby.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

/**
 * 
 * @author Daniel Wood
 *
 */
public class LogoutScreen implements Screen {

	RoombyGDX game;
	Stage stage;
	SpriteBatch batch;

	
	public LogoutScreen(RoombyGDX game)
	{
		this.game = game;
		
	}
	public LogoutScreen(RoombyGDX game, Stage stage)
	{
		this.game = game;
		this.stage = stage;
		
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0.59f, 0.84f, 1f, 1);
		stage.act(delta);
		stage.draw();
		
		if(Gdx.input.isKeyPressed(Keys.BACK))
		{
			game.setScreen(game.dashboardScreen);
		}
		else if(Gdx.input.justTouched())
		{
			game.logout();
		
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		stage = new Stage();
		Label confirm = new Label("Log-out?\nTap to confirm\n- or -\nPress 'Back' to return",RoombySkin.getLabelStyle(88));
		confirm.setColor(Color.BLACK);
		confirm.setPosition(Gdx.graphics.getWidth()/2-(confirm.getWidth()/2),
				            Gdx.graphics.getHeight()/2-(confirm.getHeight()/2));
		confirm.setAlignment(Align.center);
		stage.addActor(confirm);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		show();
	}

	@Override
	public void dispose() {
		stage.dispose();
//		Assets.disposeAll();
//		batch.dispose();
//		if (mHelper != null)
//			mHelper.onStop();
//		if (batch != null){
//			batch.dispose();
//		}
//		batch = null;
//		if (stage !=  null){
//			stage.dispose();
//		}
//		stage = null;
		
	}

}
