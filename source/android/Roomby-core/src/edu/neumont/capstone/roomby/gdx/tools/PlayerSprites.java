/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import java.io.Serializable;

public enum PlayerSprites implements Serializable{

	BROWN_BROWN("packedSprite.txt"),
	BLACK_BLACK("player_black_black.txt"),
	BLACK_RED("player_black_red.txt"),
	RED_RED("player_red_red.txt"),
	PURPLE_PURPLE("player_purple_purple.txt");
	
	private String value;
	public String getValue(){return value;}
	private PlayerSprites(String value){
		this.value = value;
	}
}
