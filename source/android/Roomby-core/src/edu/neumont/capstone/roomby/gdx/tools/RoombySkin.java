/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.utils.Json;

public class RoombySkin {


	public static Skin getSkin(){
		Skin buttonSkin;
		buttonSkin = new Skin();
		
		
		buttonSkin.add("font", getFont(42), BitmapFont.class);
		buttonSkin.add("default-button", Assets.get("button.png"));
		buttonSkin.add("default-button-pressed", Assets.get("button_pressed.png"));
		buttonSkin.add("right-arrow", Assets.get("arrow.png"));
		buttonSkin.add("right-arrow_down", Assets.get("arrow_pressed.png"));
		buttonSkin.add("left-arrow", Assets.get("arrow-left.png"));
		buttonSkin.add("left-arrow_down", Assets.get("arrow-left_pressed.png"));
		buttonSkin.add("edit-button", Assets.get("editicon.png"));
		buttonSkin.add("edit-button_down", Assets.get("editicon_pressed.png"));
		buttonSkin.add("list-background", Assets.get("listBackground.png"));
		buttonSkin.add("list-background_down", Assets.get("listBackground_pressed.png"));
		buttonSkin.add("toggle-on", Assets.get("toggleOn.png"));
		buttonSkin.add("toggle-off", Assets.get("toggleOff.png"));
		buttonSkin.add("home-button", Assets.get("home.png"));
		buttonSkin.add("home-button_pressed", Assets.get("home_pressed.png"));
		buttonSkin.add("popup-background", Assets.get("popup_background.png"));

		TextButtonStyle tbs = new TextButtonStyle();
		tbs.up = buttonSkin.getDrawable("default-button");
		tbs.down = buttonSkin.getDrawable("default-button-pressed");
		tbs.font = buttonSkin.getFont("font");	
		buttonSkin.add("default", tbs, TextButtonStyle.class);
		
		ImageButtonStyle right_ibs, left_ibs, edit_ibs, toggle_ibs, home_ibs;
		TextButtonStyle toggle_bs;

		TextFieldStyle writing_ts = new TextFieldStyle();
		writing_ts.font = getFont(66);
		writing_ts.fontColor = Color.BLACK;
		
		WindowStyle popup = new WindowStyle();
		popup.background = buttonSkin.getDrawable("popup-background");
		popup.titleFont = getFont(88);
		
		
		right_ibs = new ImageButtonStyle();
		right_ibs.up = buttonSkin.getDrawable("right-arrow");
		right_ibs.down = buttonSkin.getDrawable("right-arrow_down");
		
		left_ibs = new ImageButtonStyle();
		left_ibs.up = buttonSkin.getDrawable("left-arrow");
		left_ibs.down = buttonSkin.getDrawable("left-arrow_down");
		
		edit_ibs = new ImageButtonStyle();
		edit_ibs.up = buttonSkin.getDrawable("edit-button");
		edit_ibs.checked = buttonSkin.getDrawable("edit-button_down");
		
		toggle_ibs = new ImageButtonStyle();
		toggle_ibs.up = buttonSkin.getDrawable("toggle-on");
		toggle_ibs.checked = buttonSkin.getDrawable("toggle-off");
		
		home_ibs = new ImageButtonStyle();
		home_ibs.up = buttonSkin.getDrawable("home-button");
		home_ibs.down = buttonSkin.getDrawable("home-button_pressed");
		
		toggle_bs = new TextButtonStyle();
		toggle_bs.up = buttonSkin.getDrawable("toggle-on");
		toggle_bs.checked = buttonSkin.getDrawable("toggle-off");
		toggle_bs.font = getFont(66);
		
		SelectBoxStyle sbs = new SelectBoxStyle();
		sbs.font = getFont(42);
		sbs.fontColor = Color.WHITE;
		sbs.background = buttonSkin.getDrawable("list-background");
		sbs.backgroundOver = buttonSkin.getDrawable("list-background_down");
//		sbs.
		
		buttonSkin.add("sbs", sbs);
		buttonSkin.add("lbs",getLabelStyle(48));
		buttonSkin.add("default",getLabelStyle(66));
		buttonSkin.add("right-arrow-button-style", right_ibs);
		buttonSkin.add("left-arrow-button-style", left_ibs);
		buttonSkin.add("edit-button-style", edit_ibs);
		buttonSkin.add("toggleHanded", toggle_bs);
		buttonSkin.add("home-button-style", home_ibs);
		buttonSkin.add("popup-style", popup);
		buttonSkin.add("writing-field-style", writing_ts);
//		new Json().prettyPrint(buttonSkin);
		return buttonSkin;
	}

	public static LabelStyle getLabelStyle(int size)
	{
		LabelStyle lbs = new LabelStyle();
		lbs.font = getFont(size);
		return lbs;
	}
	public static BitmapFont getFont(int size)
	{
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Aerospace.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		BitmapFont font = generator.generateFont(parameter); 
		font.setColor(Color.WHITE);
		generator.dispose(); // don't forget to dispose to avoid memory leaks!
		
		return font;
	}

	/**
	 * @return
	 */
	public static ScrollPaneStyle getScrollPaneStyle() {
		
		ScrollPaneStyle style = new ScrollPaneStyle();
		style.background = Assets.getSkin().getDrawable("list-background");
		return style;
	}
}
