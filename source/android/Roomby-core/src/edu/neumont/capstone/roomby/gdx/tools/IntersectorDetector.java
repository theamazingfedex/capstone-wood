/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import android.util.Log;

import com.badlogic.gdx.scenes.scene2d.Actor;


public class IntersectorDetector {

	public static boolean detect(Actor player, Actor object)
	{
		boolean overlapping = false;

		if (player.getCenterX() >= object.getX()
		 && player.getCenterX() <= object.getX()+object.getWidth()
		 && player.getCenterY() >= object.getY()
		 && player.getCenterY() <= object.getY()+object.getHeight())
			overlapping = true;
		else if (player.getX() >= object.getX()
				 && player.getX() <= object.getX()+object.getWidth()
				 && player.getY() >= object.getY()
				 && player.getY() <= object.getY()+object.getHeight())
					overlapping = true;
		else if (player.getX()+player.getWidth() >= object.getX()
				 && player.getX()+player.getWidth() <= object.getX()+object.getWidth()
				 && player.getY()+player.getHeight() >= object.getY()
				 && player.getY()+player.getHeight() <= object.getY()+object.getHeight())
					overlapping = true;
		else if (player.getX() >= object.getX()
				 && player.getX() <= object.getX()+object.getWidth()
				 && player.getY()+player.getHeight() >= object.getY()
				 && player.getY()+player.getHeight() <= object.getY()+object.getHeight())
					overlapping = true;
		else if (player.getX()+player.getWidth() >= object.getX()
				 && player.getX()+player.getWidth() <= object.getX()+object.getWidth()
				 && player.getY() >= object.getY()
				 && player.getY() <= object.getY()+object.getHeight())
					overlapping = true;
//		Log.d("Intersector Detector", "detecting... playerX: "+player.getCenterX()+ " PlayerY: "+player.getCenterY()
//				+"\nObjectName:"+object.getName()+"\nobjectX1: "+object.getX()+" ObjectY1: "+object.getY()
//				+"\nObjectX2: "+object.getX()+object.getWidth()+" ObjectY2: "+object.getY()+object.getHeight());
		return overlapping;
	}
}
