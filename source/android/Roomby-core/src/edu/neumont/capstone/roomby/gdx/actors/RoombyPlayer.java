package edu.neumont.capstone.roomby.gdx.actors;

import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;

import edu.neumont.capstone.roomby.gdx.tools.MyColor;

public class RoombyPlayer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8422940218462895417L;
	
	MyActorConfig actorConfig = null;
	private MyColor gameScreenBackgroundColor = MyColor.BLUE; //Default blue
	private MyColor controlPadBackgroundColor = MyColor.GRAY; //Default gray
	private ArrayList<ObjectActorConfig> objects;

	private boolean isRightHanded = true; //defaults to right-handed

	
	public boolean isRightHanded() {
		return isRightHanded;
	}

	public void setRightHanded(boolean isRightHanded) {
		this.isRightHanded = isRightHanded;
	}

	public RoombyPlayer(MyActorConfig config)
	{
		this.actorConfig = config;
	}
	
	public RoombyPlayer(){}
	

	public MyActorConfig getActor()
	{
		return this.actorConfig;
	}


	public MyActorConfig getActorConfig()
	{
		return this.actorConfig;
	}
	public MyActorConfig setActorConfig(MyActorConfig actor) {
		this.actorConfig = actor;
		return actorConfig;
	}

	public ArrayList<ObjectActorConfig> getObjects() {
		if (objects == null)
			objects = new ArrayList<ObjectActorConfig>();
		return objects;
	}
	public void setObjects(ArrayList<ObjectActorConfig> objects)
	{
		this.objects = objects;
	}

	public MyColor getGameScreenBackgroundColor() {
		return gameScreenBackgroundColor;
	}

	public void setGameScreenBackgroundColor(MyColor gameScreenBackgroundColor) {
		this.gameScreenBackgroundColor = gameScreenBackgroundColor;
	}

	public MyColor getControlPadBackgroundColor() {
		return controlPadBackgroundColor;
	}

	public void setControlPadBackgroundColor(MyColor controlPadBackgroundColor) {
		this.controlPadBackgroundColor = controlPadBackgroundColor;
	}

}
