package edu.neumont.capstone.roomby.gdx.tools;


/**
 * 
 * @author Daniel Wood
 *    
 *  <string name="achievement_firstdown">CgkI8_SJsaAMEAIQAw</string>
    <string name="achievement_closeencounters">CgkI8_SJsaAMEAIQBA</string>
    <string name="achievement_newspender">CgkI8_SJsaAMEAIQBQ</string>
    <string name="achievement_bigspender">CgkI8_SJsaAMEAIQBg</string>
    <string name="achievement_vagabond">CgkI8_SJsaAMEAIQBw</string>
 */
public enum Achievements {
	FIRST_DOWN("CgkI8_SJsaAMEAIQAw"),
	CLOSE_ENCOUNTERS("CgkI8_SJsaAMEAIQBA"),
	NEW_SPENDER("CgkI8_SJsaAMEAIQBQ"),
	BIG_SPENDER("CgkI8_SJsaAMEAIQBg"),
	VAGABOND("CgkI8_SJsaAMEAIQBw");
	private String	value;
	public String getValue(){return value;}
	private Achievements(String value){
		this.value = value;
	}
}