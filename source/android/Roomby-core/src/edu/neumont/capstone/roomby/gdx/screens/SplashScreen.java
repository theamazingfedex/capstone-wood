/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.screens;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.MyColor;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

/**
 * Shows a splash image and moves on to the next screen.
 */
public class SplashScreen
   implements Screen
{
	private Stage stage;
	private RoombyGDX game;
    private Texture splashTexture;
	private FitViewport viewport;
	private Camera camera;
	private boolean ran;
	private boolean started;
	protected boolean ready = false;
	private boolean startLoading = false;
	private AssetManager manager;
    public SplashScreen(
        RoombyGDX game )
    {
        this.game = game;
        manager = Assets.getInstance();
    }

    @Override
    public void show()
    {
    	game.resolver.onPostCreate();
    	
//    	Assets.finishLoading();
    	ran = false;
    	started = false;
    	stage = new Stage();
    	Gdx.input.setInputProcessor(stage);
    	camera = stage.getCamera();
    	viewport = new FitViewport(1080, 1920, camera);

        splashTexture = new Texture(Gdx.files.internal("play.png")); 
        		//game.resolver.assetManager.get("play.png", Texture.class);

        TextureRegion splashRegion = new TextureRegion(splashTexture);
        Image splashImage = new Image(splashRegion);
        splashImage.setWidth(500);
        splashImage.setHeight(500);
        splashImage.setX((Gdx.graphics.getWidth()/2)-(splashImage.getWidth()/2));
        splashImage.setY((Gdx.graphics.getHeight()/2)-(splashImage.getHeight()/2));
        
        Label lab = new Label("Roomby Social Network", RoombySkin.getLabelStyle(82));
        lab.scaleBy(8);
        lab.setPosition((Gdx.graphics.getWidth()/2)-(lab.getWidth()/2),
        		splashImage.getY()+splashImage.getHeight()+10);
        
        stage.addActor(lab);
        stage.addActor(splashImage);
        
        Assets.queueLoading();
        
        
//        splashImage.addAction(Actions.sequence(Actions.alpha(0)
//                ,Actions.fadeIn(0.75f),Actions.delay(1.5f),Actions.run(new Runnable() {
//            @Override
//            public void run() {
//                game.setScreen(game.dashboardScreen);
//            }
//        })));
        
    }

    @Override
    public void resize(
        int width,
        int height )
    {
    	viewport.setScaling(Scaling.fit);
		viewport.update(width, height);

    }

    @Override
    public void dispose()
    {
//        super.dispose();
//        splashTexture.dispose();
    	stage.dispose();
//    	splashTexture.dispose();
    	
    	
    }

	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(MyColor.BLUE.getValue().r,
							MyColor.BLUE.getValue().g, 
							MyColor.BLUE.getValue().b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
//		if (!Assets.isLoaded("sign.png")){
//			Assets.update();
//			return;
//		}
//		{
//		if (Assets.update())
//		if (Gdx.app.getPreferences("AUTO_SIGN_IN").getBoolean("finishedShowing"))
		if (!game.androidApp.isRestricted())
		if (game.isSignedIn())
		{

			if (!(manager.getAssetNames().size > 1)){
				Log.e("ASSETS", "Assets are smaller than 1");
				manager.update();
//				manager.finishLoading();
			}
			
				Log.e("Update Assets from Splash", "Updated assets!!!");
					if(!ready)
					{
//						Assets.getSkin();
						ready = true;
						Timer.schedule(new Timer.Task() {
	
							@Override
							public void run() {
								
								game.setScreen(game.dashboardScreen);
							}
						}, 2);
					}
				}
			
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
//		render(Gdx.graphics.getDeltaTime());
	}
}