/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.screens;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.actors.MyActor;
import edu.neumont.capstone.roomby.gdx.actors.MyActorConfig;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.MyColor;
import edu.neumont.capstone.roomby.gdx.tools.PlayerSprites;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

public class UserOptionsScreen implements Screen {

	Stage stage;
	RoombyGDX game;
	private int screenWidth;
	private int screenHeight;
	private MyActor curActor;
	private int spriteIterator;
	private Vector2 oldPosition;
	private Actor backgroundSplotch;
	private Actor controlpadSplotch;
	private MyColor curBgColor;
	private MyActorConfig config;
	private MyColor curCpColor;
	private LabelStyle optionsLabelStyle;
	private TextButton toggleButton;
	protected boolean isRightHanded;
	
	public UserOptionsScreen(RoombyGDX game)
	{
		this.stage = new Stage();
		this.game = game;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		config = game.getCurPlayer().getActorConfig();
		curActor = new MyActor(config);
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		oldPosition = new Vector2(curActor.getX(),curActor.getY());
		curActor.setPosition((screenWidth/2)-(curActor.getWidth()/2), screenHeight*0.75f);
		curBgColor = game.getCurPlayer().getGameScreenBackgroundColor();
		curCpColor = game.getCurPlayer().getControlPadBackgroundColor();
		optionsLabelStyle = RoombySkin.getLabelStyle(66);
		isRightHanded = game.getCurPlayer().isRightHanded();
		final float bgIconX = curActor.getCenterX()-(curActor.getHeight()/2);
		final float bgIconY = curActor.getY()-(curActor.getHeight()*2.5f);
		final float bgIconWidth = curActor.getHeight();
		final float bgIconHeight = curActor.getHeight();
		final int shade = 4;
		backgroundSplotch = new Actor(){
			ShapeRenderer sr = new ShapeRenderer();
			@Override
			public void draw(Batch batch, float delta)
			{
				batch.end();
				sr.begin(ShapeType.Filled);
				sr.setColor(Color.BLACK);
				sr.rect(bgIconX-shade,bgIconY-shade,bgIconWidth+shade,bgIconHeight+shade);
				sr.end();
				sr.begin(ShapeType.Filled);
				sr.setColor(curBgColor.getValue());
				sr.rect(bgIconX,bgIconY,bgIconWidth,bgIconHeight);
				sr.end();
				
				batch.begin();
			}
		};
		backgroundSplotch.setBounds(bgIconX-shade,bgIconY-shade,bgIconWidth+2*shade,bgIconHeight+2*shade);
		
		final float cpIconX = curActor.getCenterX()-(curActor.getHeight()/2);
		final float cpIconY = curActor.getY()-(curActor.getHeight()*5f);
		final float cpIconWidth = curActor.getHeight();
		final float cpIconHeight = curActor.getHeight();
		controlpadSplotch = new Actor(){
			ShapeRenderer sr = new ShapeRenderer();
			@Override
			public void draw(Batch batch, float delta)
			{
				batch.end();
				sr.begin(ShapeType.Filled);
				sr.setColor(Color.BLACK);
				sr.rect(cpIconX-shade,cpIconY-shade,cpIconWidth+shade,cpIconHeight+shade);
				sr.end();
				sr.begin(ShapeType.Filled);
				sr.setColor(curCpColor.getValue());
				sr.rect(cpIconX,cpIconY,cpIconWidth,cpIconHeight);
				sr.end();
				
				batch.begin();
			}
		};
		controlpadSplotch.setBounds(cpIconX-shade,cpIconY-shade,cpIconWidth+2*shade,cpIconHeight+2*shade);
		
		toggleButton = new TextButton("Toggle Handed", Assets.getSkin().get("toggleHanded", TextButtonStyle.class));
		toggleButton.setSize(450, 150);
		
		final float toggleX = curActor.getCenterX()-(toggleButton.getWidth()/2);
		final float toggleY = curActor.getY()-(curActor.getHeight()*7.5f);
		toggleButton.setPosition(toggleX, toggleY);
		
//		toggleButton.setText("Right");
		toggleButton.addListener(new ClickListener(){

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				if (isRightHanded)
				{
					isRightHanded  = false;
					toggleButton.setText("Left");
					return false;
				}
				else
					return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				isRightHanded = true;
				toggleButton.setText("Right");
			}
		});
		toggleButton.setChecked(!isRightHanded);
		if (isRightHanded)
			toggleButton.setText("Right");
		else
			toggleButton.setText("Left");
		showOptions();
		showButtons();
		
		stage.addActor(toggleButton);
		stage.addActor(controlpadSplotch);
		stage.addActor(backgroundSplotch);
		stage.addActor(curActor);
		
		
		
		
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
	}

	private void showOptions()
	{
		
		Label optionsLabel = new Label("Roomby Options",RoombySkin.getLabelStyle(88));
		optionsLabel.setPosition(curActor.getCenterX() - (optionsLabel.getWidth()/2),
								 curActor.getCenterY() + (optionsLabel.getHeight()*2));
		
		Label playerSpriteLabel = new Label("Player", optionsLabelStyle);
		playerSpriteLabel.setPosition(curActor.getCenterX() - (playerSpriteLabel.getWidth()/2), 
					                  curActor.getCenterY() - (playerSpriteLabel.getHeight()*1.75f));
		
		Label bgColorLabel = new Label("Game Background", optionsLabelStyle);
		bgColorLabel.setPosition(curActor.getCenterX() - (bgColorLabel.getWidth()/2),
				   				 backgroundSplotch.getCenterY() - (bgColorLabel.getHeight()*2));
		
		Label cpColorLabel = new Label("Control-Pad Background", optionsLabelStyle);
		cpColorLabel.setPosition(curActor.getCenterX() - (cpColorLabel.getWidth()/2),
								 controlpadSplotch.getCenterY() - (cpColorLabel.getHeight()*2));
		
		Label toggleLabel = new Label("Toggle Controller Orientation", optionsLabelStyle);
		toggleLabel.setPosition(curActor.getCenterX() - (toggleLabel.getWidth()/2),
								toggleButton.getCenterY() - (toggleLabel.getHeight()*1.75f));
		
		stage.addActor(toggleLabel);
		stage.addActor(bgColorLabel);
		stage.addActor(cpColorLabel);
		stage.addActor(playerSpriteLabel);
		stage.addActor(optionsLabel);
	}
	private void showButtons() 
	{
		ImageButton leftArrow = 
				new ImageButton(
						Assets.getSkin().get(
								"left-arrow-button-style", ImageButtonStyle.class));
		leftArrow.setHeight(curActor.getHeight());
		leftArrow.setWidth(curActor.getHeight());
		leftArrow.setPosition(curActor.getCenterX()-((leftArrow.getWidth()+curActor.getWidth())*2)
							, curActor.getY());
		
		leftArrow.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleLeft();
				return false;
			}
		});
		ImageButton rightArrow = 
				new ImageButton(
						Assets.getSkin().get(
								"right-arrow-button-style", ImageButtonStyle.class));
		rightArrow.setHeight(curActor.getHeight());
		rightArrow.setWidth(curActor.getHeight());
		rightArrow.setPosition(curActor.getCenterX()+(leftArrow.getWidth()*2)
							, curActor.getY());
		rightArrow.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleRight();
				return false;
			}
		});
		
		ImageButton leftArrowForBgColor = 
				new ImageButton(
						Assets.getSkin().get(
								"left-arrow-button-style", ImageButtonStyle.class));
		leftArrowForBgColor.setHeight(curActor.getHeight());
		leftArrowForBgColor.setWidth(curActor.getHeight());
		leftArrowForBgColor.setPosition(backgroundSplotch.getCenterX()-((leftArrow.getWidth()+curActor.getWidth())*2)
							, backgroundSplotch.getY());
		
		leftArrowForBgColor.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleBgLeft();
				return false;
			}
		});
		ImageButton rightArrowForBgColor = 
				new ImageButton(
						Assets.getSkin().get(
								"right-arrow-button-style", ImageButtonStyle.class));
		rightArrowForBgColor.setHeight(curActor.getHeight());
		rightArrowForBgColor.setWidth(curActor.getHeight());
		rightArrowForBgColor.setPosition(curActor.getCenterX()+(leftArrow.getWidth()*2)
							, backgroundSplotch.getY());
		rightArrowForBgColor.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleBgRight();
				return false;
			}
		});
		
		ImageButton leftArrowForCpColor = 
				new ImageButton(
						Assets.getSkin().get(
								"left-arrow-button-style", ImageButtonStyle.class));
		leftArrowForCpColor.setHeight(curActor.getHeight());
		leftArrowForCpColor.setWidth(curActor.getHeight());
		leftArrowForCpColor.setPosition(curActor.getCenterX()-((leftArrow.getWidth()+curActor.getWidth())*2)
							, controlpadSplotch.getY());
		
		leftArrowForCpColor.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleCpLeft();
				return false;
			}
		});
		ImageButton rightArrowForCpColor = 
				new ImageButton(
						Assets.getSkin().get(
								"right-arrow-button-style", ImageButtonStyle.class));
		rightArrowForCpColor.setHeight(curActor.getHeight());
		rightArrowForCpColor.setWidth(curActor.getHeight());
		rightArrowForCpColor.setPosition(curActor.getCenterX()+(leftArrow.getWidth()*2)
							, controlpadSplotch.getY());
		rightArrowForCpColor.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				cycleCpRight();
				return false;
			}
		});
		
		stage.addActor(rightArrow);
		stage.addActor(leftArrow);
		stage.addActor(rightArrowForBgColor);
		stage.addActor(leftArrowForBgColor);
		stage.addActor(rightArrowForCpColor);
		stage.addActor(leftArrowForCpColor);
		
	}

	private void update(float delta)
	{
		
//		curActor.clear();
		
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(MyColor.ORANGE.getValue().r,
							MyColor.ORANGE.getValue().g, 
							MyColor.ORANGE.getValue().b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(Gdx.input.isKeyPressed(Keys.BACK))
		{
			Log.d("UserProfile BACK", "setting DashboardScreen and updating game.playeractor");
			curActor.setPosition(oldPosition.x, oldPosition.y);
			
			game.updatePreferences(curBgColor, curCpColor, isRightHanded);
			game.updatePlayerActor(curActor.getConfig());
			game.setScreen(game.dashboardScreen);
		}
		if(Gdx.input.isKeyPressed(Keys.MENU))
		{
			spriteIterator++;
		}
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	private void cycleCpRight()
	{
		if (curCpColor == MyColor.BLUE)
			curCpColor = MyColor.ORANGE;
		else if (curCpColor == MyColor.ORANGE)
			curCpColor = MyColor.GREEN;
		else if (curCpColor == MyColor.GREEN)
			curCpColor = MyColor.RED;
		else if (curCpColor == MyColor.RED)
			curCpColor = MyColor.GRAY;
		else if (curCpColor == MyColor.GRAY)
			curCpColor = MyColor.BLUE;
		
	}
	private void cycleCpLeft()
	{
		if (curCpColor == MyColor.BLUE)
			curCpColor = MyColor.GRAY;
		else if (curCpColor == MyColor.GRAY)
			curCpColor = MyColor.RED;
		else if (curCpColor == MyColor.RED)
			curCpColor = MyColor.GREEN;
		else if (curCpColor == MyColor.GREEN)
			curCpColor = MyColor.ORANGE;
		else if (curCpColor == MyColor.ORANGE)
			curCpColor = MyColor.BLUE;
	}
	private void cycleBgRight()
	{
		if (curBgColor == MyColor.BLUE)
			curBgColor = MyColor.ORANGE;
		else if (curBgColor == MyColor.ORANGE)
			curBgColor = MyColor.GREEN;
		else if (curBgColor == MyColor.GREEN)
			curBgColor = MyColor.RED;
		else if (curBgColor == MyColor.RED)
			curBgColor = MyColor.PURPLE;
		else if (curBgColor == MyColor.PURPLE)
			curBgColor = MyColor.BLUE;
		
	}
	private void cycleBgLeft()
	{
		if (curBgColor == MyColor.BLUE)
			curBgColor = MyColor.PURPLE;
		else if (curBgColor == MyColor.PURPLE)
			curBgColor = MyColor.RED;
		else if (curBgColor == MyColor.RED)
			curBgColor = MyColor.GREEN;
		else if (curBgColor == MyColor.GREEN)
			curBgColor = MyColor.ORANGE;
		else if (curBgColor == MyColor.ORANGE)
			curBgColor = MyColor.BLUE;
	}
	private void cycleRight()
	{
		MyActorConfig tempConfig = null;
		switch(curActor.getConfig().spritesheet)
		{
		case BROWN_BROWN:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_BLACK, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case BLACK_BLACK:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case BLACK_RED:
			tempConfig = new MyActorConfig(PlayerSprites.RED_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case RED_RED:
			tempConfig = new MyActorConfig(PlayerSprites.PURPLE_PURPLE, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case PURPLE_PURPLE:
			tempConfig = new MyActorConfig(PlayerSprites.BROWN_BROWN, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		default:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		}
		curActor.updateConfig(tempConfig);
		curActor.setPosition((screenWidth/2)-(curActor.getWidth()/2), screenHeight*0.75f);
	}
	private void cycleLeft()
	{
		MyActorConfig tempConfig = null;
		switch(curActor.getConfig().spritesheet)
		{
		case BROWN_BROWN:
			tempConfig = new MyActorConfig(PlayerSprites.PURPLE_PURPLE, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case BLACK_BLACK:
			tempConfig = new MyActorConfig(PlayerSprites.BROWN_BROWN, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case BLACK_RED:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_BLACK, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case RED_RED:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		case PURPLE_PURPLE:
			tempConfig = new MyActorConfig(PlayerSprites.RED_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		default:
			tempConfig = new MyActorConfig(PlayerSprites.BLACK_RED, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
			break;
		}
		curActor.updateConfig(tempConfig);
		curActor.setPosition((screenWidth/2)-(curActor.getWidth()/2), screenHeight*0.75f);
	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.clear();
		stage.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		

	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}
}