/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class Assets {
	public static AssetManager manager = new AssetManager();
	public static Skin roombySkin = null;
	public static FileHandle allItemsFile = null;
	
	public static void queueLoading()
	{
		allItemsFile = Gdx.files.internal("edu.neumont.capstone.roomby.userdata.txt.items");
//		manager.load("fonts/Aerospace.ttf", FileHandle.class);
//		manager.load("fonts/GoodDog.otf", FileHandle.class);
//		manager.load("fonts/Pacifico.otf", FileHandle.class);
//		manager.load("edu.neumont.capstone.roomby.userdata.txt.items", String.class);
		manager.load("play.png", Texture.class);
		manager.load("dpad.png", Texture.class);
		manager.load("touchKnob.png", Texture.class);
		manager.load("bucket.png", Texture.class);
		
		manager.load("bed_01.png", Texture.class);

		
		manager.load("bookcase.png", Texture.class);
		manager.load("bookcase_01.png", Texture.class);
		manager.load("bookcase_02.png", Texture.class);
		manager.load("bookcase_03.png", Texture.class);
		
		manager.load("computer_01.png", Texture.class);
		
		manager.load("red_couch.png", Texture.class);
		
		manager.load("packedSprite.txt", TextureAtlas.class);
		manager.load("player_black_black.txt", TextureAtlas.class);
		manager.load("player_black_red.txt", TextureAtlas.class);
		manager.load("player_red_red.txt", TextureAtlas.class);
		manager.load("player_purple_purple.txt", TextureAtlas.class);

		manager.load("toggleOn.png", Texture.class);
		manager.load("toggleOff.png", Texture.class);
		manager.load("listBackground_pressed.png", Texture.class);
		manager.load("listBackground.png", Texture.class);
		manager.load("editicon_pressed.png", Texture.class);
		manager.load("editicon.png", Texture.class);
		manager.load("arrow-left_pressed.png", Texture.class);
		manager.load("arrow-left.png", Texture.class);
		manager.load("arrow_pressed.png", Texture.class);
		manager.load("arrow.png", Texture.class);
		manager.load("button_pressed.png", Texture.class);
		manager.load("button.png", Texture.class);
		manager.load("home.png", Texture.class);
		manager.load("home_pressed.png", Texture.class);
		manager.load("computer_02.png", Texture.class);
		manager.load("picture_01.png", Texture.class);
		manager.load("picture_02.png", Texture.class);
		manager.load("tv_01.png", Texture.class);
		manager.load("popup_background.png", Texture.class);
//		manager.load("roombySkin.json", Skin.class);
		manager.load("sign.png", Texture.class);
//		manager.finishLoading();
	}
	// returns true if finished loading assets
	public static boolean update()
	{
		return manager.update();
	}

	public static void resetManager()
	{
		manager = new AssetManager();
	}
	public static boolean isLoaded(String filename)
	{
		return manager.isLoaded(filename);
	}
	public static void finishLoading()
	{
		manager.finishLoading();
	}
	public static AssetManager getInstance()
	{
		return manager;
	}
	public static Skin getSkin()
	{
		if (roombySkin == null)
			roombySkin = RoombySkin.getSkin();
		return roombySkin;
	}
	public static <T> T get(String filename, Class<T> classtype)
	{
		T t = null;
		if (update())
		{
			t = manager.get(filename, classtype);
		}
		return t;
			
	}
	public static <T> T get(String filename)
	{
		T t = null;
		if (update())
		{
			t = manager.get(filename);
		}
		return t;
			
	}
}
