package edu.neumont.capstone.roomby.gdx.tools;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.example.games.basegameutils.GameHelper;

import edu.neumont.capstone.roomby.gdx.actors.MyActorConfig;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActorConfig;
import edu.neumont.capstone.roomby.gdx.actors.RoombyPlayer;

/**
 * 
 * @author Daniel Wood
 *
 */
public class AndroidActionResolver implements IActionResolver, GameHelper.GameHelperListener 
{
    private ArrayList<IActionListener> listeners;
    private final static int RESULT_LB_SCORE = 100;
    private final static int RESULT_LB_ACHIEV = 101;
    private final static int RESULT_LB_SETTINGS = 102;
    private static final String MAX_SCORE = "MAX_SCORE";
    private static final String SIGNED_IN = "SIGNED_IN";
    private static final String AUTO_SIGN_IN = "AUTO_SIGN_IN";
    private static String filename = null;
    private static String fullfilename = "";

    private GameHelper gh;
    private Activity activity;
    private com.badlogic.gdx.Preferences preferences;
    private Tracker gameTracker;
	private Context appContext;
	private Json json;
	
	public AssetManager assetManager;


    public AndroidActionResolver(Context appContext, Activity activity){
        this.activity = activity;
        this.appContext = appContext;
        assetManager = new AssetManager();
        gh = new GameHelper(activity, GameHelper.CLIENT_ALL);
        gh.setup(this);
        gh.enableDebugLog(true);
//        activity.set
        listeners = new ArrayList<IActionListener>();
        
//        ADD USER SPECIFIC NAME TO FILEPATH FILENAME
       
        json = new Json();
    	json.setTypeName(null);
    	json.setUsePrototypes(false);
    	json.setIgnoreUnknownFields(true);
    	json.setOutputType(OutputType.json);
        
    }

    public AssetManager getAssetManager()
    {
    	return assetManager;
    }
    
    public ArrayList<ObjectActorConfig> loadAllItems()
    {
    	if (filename == null)
    		setupFilename();
    	ArrayList<ObjectActorConfig> items = new ArrayList<ObjectActorConfig>();
    	ArrayList<JsonValue> tempItems = null;
    	ObjectActorConfig tempItem = null;
    	try {
    		FileHandle file = Assets.allItemsFile;
    		String jsonData = file.readString();
//    		String jsonData = Assets.get("edu.neumont.capstone.roomby.userdata.txt.items", String.class);
//    		Log.e("ActionResolver LOADING ITEMS", jsonData);
    		tempItems = json.fromJson(ArrayList.class, jsonData);
    		
    		for (JsonValue item : tempItems)
    		{
    			tempItem = json.fromJson(ObjectActorConfig.class, item.toString());
    			items.add(tempItem);
    		}
		} catch (GdxRuntimeException e) {
//			items = new ArrayList<ObjectActorConfig>();
//			items.add(new ObjectActorConfig("bookcase.png", 540, 960, ObjectTypes.SIMPLE_OBJECT, true, 3));
//			savePlayerItems(items);
			Log.e("ActionResolver LOADING ITEMS", "Failed to load items.");
		}
    	
    	return items;
    }
    public void saveUserData(RoombyPlayer data)
    {
    	if (filename == null)
    		setupFilename();
    	String jsonString = json.toJson(data, RoombyPlayer.class);
    	FileHandle file = Gdx.files.local(filename);
    	file.writeString(jsonString, false);   // True means append, false means overwrite.
//    	Log.i("ActionResolver SAVING",jsonString);
    	
    }
    
    public RoombyPlayer loadUserData()
    {
    	if (filename == null)
    		setupFilename();
		RoombyPlayer data = new RoombyPlayer();
		setupFilename();
		try {
		FileHandle file = Gdx.files.local(filename);
		String jsonData = file.readString();
//		Log.i("ActionResolver", jsonData);
		data = new Json().fromJson(RoombyPlayer.class, jsonData);
		} catch (GdxRuntimeException e) {
			data = new RoombyPlayer(new MyActorConfig(PlayerSprites.BROWN_BROWN, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2));
			saveUserData(data);
			Log.e("ActionResolver", "Failed to load user, creating new UserData.");
		}
    	return data;
    }
    public <T> T loadAsset(String filename, Class classType)
    {
    	T data = null;
    	try {
	    	data = (T) assetManager.get(filename, classType);
	    	assetManager.update();
    	} catch (Exception ex){
    		ex.printStackTrace();
    	}
    	return data;
    }
    
    public void showToast(final String message, final boolean isShownLong)
    {
    	new Handler().post(new Runnable() {
            public void run() {
                    if (isShownLong)
                    	Toast.makeText(appContext, message, Toast.LENGTH_LONG).show();
                    else
                    	Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
            }
    	});
    }
    public Player getSignedInPlayer(){	
    	Player player = null;
//    	this.onStart();
    	if (!isSignedIn())
    	{
    		player = null;
//    		signIn();
//    		player = Games.Players.getCurrentPlayer(gh.getApiClient());
    	}
    	else
    	{
//    		this.onStart();
//    		signIn();
    		player = Games.Players.getCurrentPlayer(gh.getApiClient());
//    		player = getSignedInPlayer();
    	}
//    	this.onStop();
    	return player;
    }
    private void setupFilename()
    {
    	filename ="edu.neumont.capstone.roomby.userdata"+ getSignedInPlayer().getDisplayName();
        fullfilename = appContext.getFilesDir()+filename;
    }
    public GoogleApiClient getApiClient(){
//    	gh.setup(this);
    	gh.onStart(activity);
    	return gh.getApiClient();
    }
    /**
     * Must be called after Gdx is initialized
     */
    public void onPostCreate(){
        preferences = Gdx.app.getPreferences(AUTO_SIGN_IN);
//        preferences.putBoolean(AUTO_SIGN_IN, true);
        if (preferences.getBoolean(AUTO_SIGN_IN, true)){
            gh.setConnectOnStart(true);
        } else {
            gh.setConnectOnStart(false);
        }
        
    }



    @Override
    public void sendScreenView(String screen){
        gameTracker.setScreenName(screen);
        gameTracker.send(new HitBuilders.AppViewBuilder().build());
    }



    @Override
    public boolean isGPGSAvailable() {

       return true;
    }

    @Override
    public void showLeaderBoard() {
        if (!isSignedIn()){
            signIn();
        } else {
            activity.startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(gh.getApiClient(), "CgkI8_SJsaAMEAIQCA"),
                    RESULT_LB_SCORE
            );
        }
    }

    @Override
    public void submitScore(long score) {
        if (isSignedIn()){
            Games.Leaderboards.submitScore(
                    gh.getApiClient(), "CgkI8_SJsaAMEAIQCA", score);
        } else {
            long currentScore = preferences.getLong(MAX_SCORE, 0);
            if (currentScore < score) {
                preferences.putLong(MAX_SCORE, score);
                preferences.flush();
            }
        }

    }

    @Override
    public void unlockAchievement(Achievements achievement) {
    	unlockAchievement(achievement.getValue());
    }

    private void unlockAchievement(String id){
        if (isSignedIn()) {
            Games.Achievements.unlock(gh.getApiClient(), id);
        } else {
            preferences.putBoolean(id, true);
            preferences.flush();
            // TODO maybe show toast?
        }
    }

    @Override
    public void showAchievements() {
        if (!isSignedIn()){
            signIn();
        } else {
            activity.startActivityForResult(
                    Games.Achievements.getAchievementsIntent(gh.getApiClient()),
                    RESULT_LB_ACHIEV
            );
        }
    }
    @Override
    public void showSettings() {
        if (!isSignedIn()){
            signIn();
        } else {
            activity.startActivityForResult(
                    Games.getSettingsIntent(gh.getApiClient()),
                    RESULT_LB_SETTINGS
            );
        }
    }
    @Override
    public void signIn() {
        try {
//        	activity.wait();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    gh.beginUserInitiatedSignIn();
                }
                
            });
//            activity.notify();
            filename ="edu.neumont.capstone.roomby.userdata"+ getSignedInPlayer().getDisplayName();
            fullfilename = appContext.getFilesDir()+filename;
        } catch (final Exception ex) {
            Log.e("SFG", "GPGS sign in failed "+ex);
        }
    }

    @Override
    public void signOut() {
        gh.signOut();
        preferences.putBoolean(AUTO_SIGN_IN, false);
        preferences.flush();
//        postEvent(IActionListener.SIGN_OUT);
    }

    @Override
    public boolean isSignedIn() {
    	
        return gh.isSignedIn();
    }

    public void onStart(){
//    	preferences = Gdx.app.getPreferences(AUTO_SIGN_IN);
        gh.onStart(activity);
//        preferences.putBoolean("finishedShowing", false);
    }
    public void onStop(){
        gh.onStop();
    }

    public void onActivityResult(int request, int response, Intent data) {
        gh.onActivityResult(request, response, data);
//        if (response == Activity.RESULT_OK)
//        {
//        	preferences.putBoolean("finishedShowing", true);
//            preferences.flush();
//        }
        
    }

    @Override
    public void onSignInFailed() {
//    	if (!isSignedIn())
//    	{	
//    		preferences.putBoolean("isSignedIn", false);
//    		preferences.flush();
//    	}
    }

    @Override
    public void onSignInSucceeded() 
    {
    	if (preferences == null)
    		preferences = Gdx.app.getPreferences("AUTO_SIGN_IN");
    	preferences.putBoolean("finishedShowing", true);
    	
    	preferences.putBoolean("isSignedIn", true);
        preferences.putBoolean(AUTO_SIGN_IN, true);
        preferences.flush();
        
    }


    @Override
    public void registerActionListener(IActionListener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void unRegisterActionListener(IActionListener listener) {
        listeners.remove(listener);
    }

	public void setAppContext(Context appContext) {
		this.appContext = appContext;
		
	}

//	@Override
//	public void unlockAchievement(Achievement achievement) {
//		// TODO Auto-generated method stub
//		String asdf = Achievement.
//	}
}