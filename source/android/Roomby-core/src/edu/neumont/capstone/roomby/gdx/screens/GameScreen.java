package edu.neumont.capstone.roomby.gdx.screens;

import java.util.ArrayList;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.google.gson.Gson;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.actors.MyActor;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActor;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActorConfig;
import edu.neumont.capstone.roomby.gdx.actors.RoombyPlayer;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.IntersectorDetector;
import edu.neumont.capstone.roomby.gdx.tools.ObjectTypes;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

/**
 * 
 * @author Daniel Wood
 *
 *	Base resolution of my SGS4:  W:1080 H:1920  (scaling)
 *potato for testing github!!
 *and another potato for eclipse!!
 */
public class GameScreen implements com.badlogic.gdx.Screen {
	

	MyActor playerActor;
    RoombyGDX game;
    Stage stage;
    Touchpad touchpad;
	private OrthographicCamera camera;
	private Skin touchpadSkin;
	private TouchpadStyle touchpadStyle;
	private int movementSpeed;
	private int screenWidth;
	private int screenHeight;

//	private ShapeRenderer shapeRenderer;
	private TextButton interactButton;
	private RoombyPlayer currentPlayer;
	private FitViewport viewport;
	private boolean isEditing;
	private ArrayList<ObjectActorConfig> objectConfigs;
	private Actor itemBox;
	private Array<ObjectActor> objects;
	private Array<ObjectActor> worldObjects;
	private boolean showingItems= false;
	private Table container;
	private Table scrollTable;
	private int prevWorldSize;
	private Actor validObjectZone;
	private Group objGroup;
	private Array<DragAndDrop> worldDraggers;
	private Label deleteLabel;
	private float rightControlPoint;
	private float leftControlPoint;
	private TextField writingField;
	private TextButton saveButton;
	private TextButton closeButton;
    
    
	public GameScreen(RoombyGDX game){
		this.game = game;
	}
	public GameScreen(RoombyGDX game, Stage stage){
		this.game = game;
		this.stage = stage;
		
	}

	@Override
	public void show() {
		Assets.finishLoading();
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
//		game.log("WIDTH", String.valueOf(screenWidth));
//		game.log("HEIGHT", String.valueOf(screenHeight));

		objects = game.loadItemObjects();
		
		worldObjects = game.loadPlayerItems();
		worldDraggers = new Array<DragAndDrop>();
		objGroup = new Group();
		objGroup.setTouchable(Touchable.enabled);
		leftControlPoint = (100);
		rightControlPoint = (screenWidth-400);
		
		stage.addListener(new InputListener(){
			@Override
			public boolean keyUp(InputEvent event, int keycode)
			{
				if (keycode == Keys.BACK)
					doBackStuff();
				return true;
			}
		});
		
		currentPlayer = game.getCurPlayer();
		playerActor = new MyActor(currentPlayer.getActorConfig());

		
        setupTouchpad();
        drawBasicShapes();
        stage.addActor(touchpad);  
        addUIbuttons();
        stage.addActor(objGroup);
        setupAllObjects();
        stage.addActor(playerActor);
        drawItemsList();
//        stage.addActor(objects.get(3));
        
        
//        testDragStuff();
        
        camera = (OrthographicCamera) stage.getCamera();
        camera.setToOrtho(false, screenWidth, screenHeight);
        
        viewport = new FitViewport(1080, 1920, camera);
        
        stage.setViewport(viewport);
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
	}
	private void doBackStuff()
	{
		if (!showingItems){
			isEditing = false;
			game.setScreen(game.dashboardScreen);
			game.updatePlayerActor(playerActor.getConfig());
			game.updatePlayerItems(worldObjects);
		}
		else
		{
			hideItemsList();
			
		}
	}
	private void drawBasicShapes() {
		validObjectZone = new Actor()
		{
			ShapeRenderer shapeRenderer = new ShapeRenderer();
			@Override
			public void draw(Batch batch, float delta)
			{
				batch.end();
				shapeRenderer.begin(ShapeType.Filled);
				shapeRenderer.setColor(currentPlayer.getGameScreenBackgroundColor().getValue());
				shapeRenderer.rect(0, screenHeight/4, screenWidth, screenHeight);
				shapeRenderer.end();
				batch.begin();
			}
			@Override
			public Actor hit(float x, float y, boolean touchable)
			{
				return null;
			}
		};
		stage.addActor(validObjectZone); //ObjectBackground for valid object placement recognition
		stage.addActor(new Actor() //Control-pad background
        {
			ShapeRenderer shapeRenderer = new ShapeRenderer();
        	float width = screenWidth;
			float height = screenHeight/4;
				
			@Override
			public Actor hit(float x, float y, boolean touchable)
			{
				return this;
			}
            @Override
            public void draw(Batch batch, float arg1) {
                batch.end();
                shapeRenderer.begin(ShapeType.Filled);
                shapeRenderer.setColor(currentPlayer.getControlPadBackgroundColor().getValue());
                shapeRenderer.rect(0, 0, width, height);
                shapeRenderer.end();
                batch.begin();
            }
        });
		createItemBox();
	}
	private ObjectActor copyObject (ObjectActor object)
	{
		ObjectActor copy = new ObjectActor(object.getConfig());
		return copy;
	}
	private ObjectActor checkForHit(Actor actor){
//		if (objects.size() == 0)
//			objects = game.loadItemObjects();
		ObjectActor hitObject = null;
		for (ObjectActor obj : worldObjects)
			if (IntersectorDetector.detect(actor, obj))
			{
				hitObject = obj;
				break;
			}
		
		return hitObject;
	}
	private void setupAllObjects() 
	{
		for (ObjectActor obj : worldObjects)	
		{
			addObjectToWorld(obj, true);
			Log.d("Setting Up Objects", "ObjType: "+obj.getObjectType());
		}
	}

	private void showModalDialogFor(final ObjectActor cactor)
	{
		final Dialog objDialog = new Dialog(cactor.getName(), Assets.getSkin(), "popup-style");
		objDialog.setSize(screenWidth, screenHeight/4);
		String infoString = "No Information Available...";
		objDialog.add("Item Properties").pad(10).center().row();
		if (cactor.getObjectType() == ObjectTypes.SIMPLE_OBJECT)
		{
			infoString = "This is a simple object, not much to do here!";
			objDialog.add(infoString).row();
			
		}		
		else if (cactor.getObjectType() == ObjectTypes.WRITING_OBJECT)
		{
			writingField = new TextField(cactor.getWriteField(), Assets.getSkin(), "writing-field-style");
			writingField.setSize(objDialog.getWidth()-150, objDialog.getHeight()/4);
			writingField.setPosition(objDialog.getCenterX()-(writingField.getWidth()/2),
									 objDialog.getCenterY());
//			ADD THE BACKGROUND TO THE WRITING FIELD
			saveButton = new TextButton("Save", Assets.getSkin().get("toggleHanded", TextButtonStyle.class));
			saveButton.setPosition(objDialog.getCenterX(), objDialog.getY()+saveButton.getHeight()*3);
			saveButton.addListener(new ClickListener(){
				@Override
				public boolean touchDown(InputEvent event, float x, float y, int button, int pointer)
				{
					cactor.setWriteField(writingField.getText());
					
					hideModalDialog(objDialog);
					return false;
				}
			});
			
			objDialog.add(writingField).height(objDialog.getHeight()/4).row();
			objDialog.add(saveButton).row();
		}
		closeButton = new TextButton("Close", Assets.getSkin().get("toggleHanded", TextButtonStyle.class));
		closeButton.setPosition(objDialog.getCenterX(), objDialog.getY()+closeButton.getHeight());
		closeButton.addListener(new ClickListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int button, int pointer)
			{
				hideModalDialog(objDialog);
				return false;
			}
		});
		objDialog.add(closeButton).row();
		objDialog.setModal(true);
		
		objDialog.show(stage);
		stage.addActor(objDialog);
		Log.e("ObjectDialog", "Showing the dialog for: "+cactor.getName());
		
	}
	private void hideModalDialog(Actor dialog)
	{
		dialog.clear();
		stage.getActors().removeValue(dialog, true);
	}
	private void showItemsList()
	{
		Log.e("ItemList", "SHOWING the items");
		showingItems = true;
		container.setVisible(true);
	}
	private void drawItemsList()
	{
		scrollTable = new Table(Assets.getSkin());
		for(ObjectActor obj : objects)
		{
			Label label = new Label(obj.getName(),RoombySkin.getLabelStyle(66));
			label.setTouchable(Touchable.disabled);
			obj.addListener(new ActorGestureListener(1,0.25f,0.5f,2){
				@Override
				public boolean longPress(Actor actor, float x, float y)
				{
					hideItemsList();
					ObjectActor temp = copyObject((ObjectActor)actor);
					temp.updatePosition(playerActor.getCenterX()-temp.getWidth()/2,
									 playerActor.getCenterY()-temp.getHeight()/2);
					addObjectToWorld(temp, false);
					
					return true;
				}
			});
			scrollTable.add(obj).pad(10);
			scrollTable.add(label).row();
		}
		
		ScrollPane pane = new ScrollPane(scrollTable);
		pane.setStyle(RoombySkin.getScrollPaneStyle());
		pane.setWidth(Gdx.graphics.getWidth()*(2/3));
		pane.setHeight(Gdx.graphics.getHeight()*(2/3));
		pane.setX(Gdx.graphics.getWidth()/2);
		pane.setY(Gdx.graphics.getHeight()/2);
		pane.setVisible(true);
		 
		container = new Table();
		hideItemsList();
		container.setWidth(Gdx.graphics.getWidth()*(2/3));
		container.setHeight(Gdx.graphics.getHeight()*(2/3));
		container.setFillParent(true);
		container.add(pane).fill().expand();
		stage.addActor(container);
	}
	private void hideItemsList()
	{
		Log.e("ItemList", "hiding the items");
//		container.clear();
//		stage.getActors().removeValue(container, true);
//		container.remove();
//		container.clearListeners();
//		container.remove();
		showingItems = false;
		container.setVisible(false);
	}
	private void addObjectToWorld(ObjectActor object, boolean isWorldObject)
	{
		object.setZIndex(1);
		object.setTouchable(Touchable.enabled);
		object.addListener(new ActorGestureListener(1,0.25f,0.5f,2){
			@Override
			public boolean longPress(Actor actor, float x, float y)
			{
//				if (isEditing){
					ObjectActor temp = (ObjectActor)actor;
					showModalDialogFor(temp);
					
					
					return true;
//				}
//				return false;
			}
		});

		if (!isWorldObject)
			worldObjects.add(object);
		objGroup.addActor(object);
//		stage.
//		Collections.sort(Arrays.asList(stage.getActors().toArray()), new ActorComparator());
//		game.updatePlayerItems(worldObjects);
	}
	
	private DragAndDrop createDragAndDrop(final ObjectActor itemObject)
	{
		DragAndDrop dragAndDrop = new DragAndDrop();
//		ObjectActor item = copyObject(itemObject);
//		final float oldX = item.getX();
//		final float oldY = item.getY();
		
		dragAndDrop.addSource(new Source(itemObject){
			
			@Override
			public Payload dragStart(InputEvent event, float x, float y,
					int pointer) {
//				Log.e("DragStaaaaarrrt", "tried to pick up item: " + getActor().getName());
				Payload payload = new Payload();
				ObjectActor cobject = itemObject;
//				cobject.setPosition(x+cobject.getX()-(cobject.getWidth()/2), y+cobject.getY()-(cobject.getHeight()/2));
//				stage.addActor(cobject);
				payload.setObject(cobject);
				payload.setDragActor(cobject);
				
//				container.setVisible(false);
				hideItemsList();
				return payload;
			}
			
			@Override
			public void dragStop (InputEvent event, float x, float y, int pointer, Payload payload, Target target)
			{
				ObjectActor temp = (ObjectActor) payload.getDragActor();
				temp.setColor(Color.WHITE);
				temp.updatePosition(x+temp.getX()-(temp.getWidth()/2), y+temp.getY()-(temp.getHeight()/2));
//				worldObjects.add(temp);
				if (checkItemForDeletion(temp))
				{
					worldObjects.removeValue(temp, true);
//					objGroup.removeActor(temp);
					stage.getActors().removeValue(temp, true);
					Log.e("Stopped dragging object", "trying to remove the object now...");
				}
				else
					addObjectToWorld(temp, true);
//				stage.addActor(temp);
//				container.swapActor(null, temp);
				Log.e("DragStahhhhp", "Tried to add item: " +temp.getName());
			}
		});

		return dragAndDrop;
	}
	private boolean checkItemForDeletion(ObjectActor obj)
	{
		return IntersectorDetector.detect(obj, itemBox);
	}
	@Override
	public void render(float delta) 
	{
		Gdx.gl.glClearColor(0.49f, 0.74f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
//		if(Gdx.input.isKeyPressed(Keys.BACK))
//		{
//			if (!showingItems)
//				game.setScreen(game.dashboardScreen);
//			else
//			{
//				hideItemsList();
//				
//			}
//		}
		
//		if (worldObjects.size > prevWorldSize)
//		{
//			prevWorldSize++;
//			
//		}
        makeMovementsFromTouchpad();
        
        stage.act(delta);
        stage.draw();
        
	}

	@Override
	public void resize(int width, int height) 
	{
		
		viewport.setScaling(Scaling.fit);
		viewport.update(width, height);
		

	}

	private void addUIbuttons()
	{
		ImageButton editButton, homeButton;// saveButton;
		
		
		interactButton = new TextButton("Interact!", Assets.getSkin(), "default");
		if (currentPlayer.isRightHanded())
			interactButton.setBounds(leftControlPoint, screenHeight/16, 300, 300);
		else
			interactButton.setBounds(rightControlPoint, screenHeight/16, 300, 300);
		interactButton.addListener(new ClickListener(0){	
			

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				game.log("0000001-RmbyGDX", "TOUCHING ALL TEH THINGS!!!!");
				ObjectActor victim = null;
				victim = checkForHit(playerActor);
				if (victim != null)
					interactWithObject(victim);
				else 
					showItemsList();
					
				
				
				return true;
			}
		});
		
		homeButton = new ImageButton(Assets.getSkin(),"home-button-style");
		homeButton.setSize(100, 100);
		homeButton.setPosition(screenWidth/2-50,
				 interactButton.getY()-(interactButton.getHeight()*0.15f));
		homeButton.addListener(new ClickListener(0){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				doBackStuff();
				return false;
			}

		});
		
		editButton = new ImageButton(Assets.getSkin(), "edit-button-style");
		editButton.setSize(100, 100);
		editButton.setPosition(screenWidth/2-45,
				 			   interactButton.getY()+(interactButton.getHeight()*0.70f));
//		saveButton = new ImageButton(RoombySkin.getSkin(), "save-button-style");
		editButton.addListener(new ClickListener(0){
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				if (!isEditing){
					isEditing = true;
					playerActor.haltMovement();
					addDraggers();
					showItemBox();
					return false;
				}
//				else{
//					
//				}
				return true;
			}
			
			

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button){
				isEditing = false;
				playerActor.enableMovement();
				removeDraggers();
				hideItemBox();
			}
		});
		
		stage.addActor(interactButton);
		stage.addActor(editButton);
		stage.addActor(homeButton);
	}
	private void addDraggers()
	{
		for (ObjectActor obj : worldObjects)
		{
			worldDraggers.add(createDragAndDrop(obj));
		}
	}
	private void removeDraggers()
	{
		for (DragAndDrop dragger : worldDraggers)
		{
			
			dragger.clear();
		}
	}
	private void interactWithObject(ObjectActor object)
	{
		switch (object.getObjectType())
		{
		case SIMPLE_OBJECT:
			Log.d("Interaction","SIMPLE_OBJECT: "+object.getName());
			break;
		case WRITING_OBJECT:
			Log.d("Interaction","WRITING_OBJECT: "+object.getName());
			break;
		case PICTURE_OBJECT:
			Log.d("Interaction","PICTURE_OBJECT: "+object.getName());
			break;
		case DRAWING_OBJECT:
			Log.d("Interaction","DRAWING_OBJECT: "+object.getName());
			break;
		default:
			Log.d("Interaction","DEFAULT_NULL_OBJECT_TYPE: "+object.getName());
			break;
		}
	}
	private void createItemBox() 
	{
		final float width = screenWidth;
		final float height = screenHeight/16;
		final float x = 0;
		final float y = screenHeight - (screenHeight/16);
		itemBox = new Actor(){
			ShapeRenderer shapeRenderer = new ShapeRenderer();
			
			
			@Override
			public void draw(Batch batch, float delta)
			{
				batch.end();
				shapeRenderer.begin(ShapeType.Filled);
                shapeRenderer.setColor(currentPlayer.getControlPadBackgroundColor().getValue());
                shapeRenderer.rect(x, y, width, height);
                shapeRenderer.end();
                
				batch.begin();
			}
		};
		itemBox.setBounds(x, y, width, height);
		itemBox.setVisible(false);
		deleteLabel = new Label("Drag Here to Delete",RoombySkin.getLabelStyle(66));
		deleteLabel.setPosition(itemBox.getCenterX()-deleteLabel.getWidth()/2, itemBox.getY());
		deleteLabel.setVisible(false);
		stage.addActor(itemBox);
		stage.addActor(deleteLabel);
	}
	private void showItemBox()
	{
		deleteLabel.setVisible(true);
		itemBox.setVisible(true);
	}
	private void hideItemBox()
	{
//		stage.getActors().removeValue(itemBox, true);
		deleteLabel.setVisible(false);
		itemBox.setVisible(false);
	}

	private void setupTouchpad()
	{
		touchpadSkin = new Skin();
        touchpadSkin.add("touchBackground", Assets.get("dpad.png"));
        touchpadSkin.add("touchKnob", Assets.get("touchKnob.png"));
        touchpadStyle = new TouchpadStyle();
        touchpadStyle.knob = touchpadSkin.getDrawable("touchKnob");
        touchpadStyle.background = touchpadSkin.getDrawable("touchBackground");
        touchpad = new Touchpad(10, touchpadStyle);
        if (currentPlayer.isRightHanded())
        	touchpad.setBounds(rightControlPoint, screenHeight/16, 300, 300);
        else
        	touchpad.setBounds(leftControlPoint, screenHeight/16, 300, 300);
        
        movementSpeed = 9;
	}
	
	private void makeMovementsFromTouchpad()
	{
		 float Xpercent = touchpad.getKnobPercentX(),
	              Ypercent = touchpad.getKnobPercentY(),
	              Xmovement = Xpercent*movementSpeed,
	              Ymovement = Ypercent*movementSpeed;
//	        Gdx.app.debug("ROOMBY-GAME", "Xpercent: "+Xpercent+"   Ypercent: "+Ypercent);
	        
		        int dir = 0;
		    	if (Xpercent > 0.5f && Ypercent > 0.5f)
		    		dir = 6;
		    	else if (Xpercent < -0.5f && Ypercent > 0.5f)
		    		dir = 5;
		    	else if (Xpercent < -0.5f && (Ypercent < 0.5f || Ypercent > -0.5f))
		    		dir = 1;
		    	else if (Ypercent > 0.5f && (Xpercent < 0.5f || Xpercent > -0.5f))
		    		dir = 2;
		    	else if (Xpercent < -0.5f && Ypercent < -0.5f)
		    		dir = 4;
		    	else if (Xpercent > 0.5f && Ypercent < -0.5f)
		    		dir = 7;
		    	else if (Xpercent > 0.5f && (Ypercent < 0.5f || Ypercent > -0.5f))
		    		dir = 3;
		    	else if (Ypercent < -0.5f && (Xpercent < 0.5f || Xpercent > -0.5f))
		    		dir = 0;
		    	
		    	playerActor.direction = dir;
		        playerActor.addAction(Actions.moveBy(Xmovement, Ymovement));
	}
	@Override
	public void hide() 
	{
//		dispose();
	}

	@Override
	public void pause() 
	{
		
	}

	@Override
	public void resume() 
	{
		
	}

	@Override
	public void dispose() 
	{
		
		if (scrollTable != null)
		{
			scrollTable.clearListeners();
			scrollTable.clear();
		}
//		try {
//			stage.getBatch().end();
//		} catch (Exception ex){}
////		stage.cl`ear();
//		currentPlayer
		stage.dispose();
//		stage.clear();
	}

}
