/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import java.io.Serializable;

public enum ObjectTypes implements Serializable{

	WRITING_OBJECT("WRITE"),
	PICTURE_OBJECT("PICTURE"),
	SIMPLE_OBJECT("SIMPLE"),
	DRAWING_OBJECT("DRAW");
	private String	value;
	public String getValue(){return value;}
	private ObjectTypes(String value){
		this.value = value;
	}
}
