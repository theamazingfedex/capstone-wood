/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

import java.io.Serializable;

import com.badlogic.gdx.graphics.Color;


public enum MyColor implements Serializable{
	BLUE(new Color(0.49f, 0.74f, 1f, 1)),
	PURPLE(new Color(0.74f, 0.49f, 1f, 1)),
	ORANGE(new Color(1f, 0.59f, 0.22f, 1)),
	RED(new Color(0.66f, 0.15f, 0.15f, 1)),
	GREEN(new Color(0.078f, 0.47f, 0.078f, 1)),
	GRAY(Color.GRAY);
	private Color value;
	public Color getValue(){return value;}
	private MyColor(Color value){
		this.value = value;
	}
}
