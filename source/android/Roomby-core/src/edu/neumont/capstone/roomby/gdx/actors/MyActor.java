package edu.neumont.capstone.roomby.gdx.actors;

import java.io.Serializable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.PlayerSprites;

/**
 * 
 * @author Daniel Wood
 *
 */
public class MyActor extends Actor implements Serializable
{
//	Texture texture = new Texture(Gdx.files.internal("data/jet.png"));
    public boolean started = false;

    private PlayerSprites playerSprite = PlayerSprites.BROWN_BROWN;
	private TextureRegion currentFrame;
	private TextureAtlas atlas;
	private AtlasRegion currentRegion, 
						up, 
						up_left, 
						up_right, 
						left, 
						right, 
						down, 
						down_left, 
						down_right;
	public int direction = 0;

	private MyActorConfig config;
	private boolean isPlayer = false;
	private float SCREEN_WIDTH;
	private float SCREEN_HEIGHT;

	private boolean canMove = true;
	
	public MyActor(){
		SCREEN_WIDTH = Gdx.graphics.getWidth();
    	SCREEN_HEIGHT =	Gdx.graphics.getHeight();
	}
	
    public MyActor(MyActorConfig actorConfig)
    {
    	SCREEN_WIDTH = Gdx.graphics.getWidth();
    	SCREEN_HEIGHT =	Gdx.graphics.getHeight();
    	updateConfig(actorConfig);
    	
    	
       
        
    }
    public MyActorConfig getConfig()
    {
    	config.x = getX();
    	config.y = getY();
    	return this.config;
    }
    
    public void updateConfig(MyActorConfig actorConfig)
	{
		this.config = actorConfig;
    	
		 setX(config.x);
	     setY(config.y);
	     setZIndex(99);
	        
	     atlas = Assets.get(config.spritesheet.getValue(), TextureAtlas.class); 
	     isPlayer = true;
		 up = atlas.findRegion("up");
		 up_left = atlas.findRegion("up_left");
		 up_right = atlas.findRegion("up_right");
		 left = atlas.findRegion("left");
		 right = atlas.findRegion("right");
		 down = atlas.findRegion("down");
		 down_left = atlas.findRegion("down_left");
		 down_right = atlas.findRegion("down_right");
		        
		 currentRegion = down; 
	       

	     if (atlas != null)
	     {
		 	setWidth(currentRegion.getRegionWidth());
		 	setHeight(currentRegion.getRegionHeight());
	     }
	     setBounds(getX(),getY(),getWidth(),getHeight());
	        
	     setTouchable(Touchable.enabled);
	     addListener(new InputListener() {
				public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) 
				{
	                Gdx.app.debug("MyActor-Debug", "MyActor.touchDown() at ("+x+", "+y+")");
	                
//	                uiThread.post(new Runnable() {
//	                    public void run() {
//	                            
//	                    	((RoombyGDX) getUserObject()).resolver.showToast("Width: "+SCREEN_WIDTH+"\nHeight: "+SCREEN_HEIGHT, true);
//	                           
//	                    }
//	            	});
	                
	                return true; 
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) 
	            {
	                Gdx.app.debug("MyActor-Debug", "MyActor.touchUp()");
	            }
	        });
	}
    @Override
    public void act(float delta){
    	if (canMove)
    		for(Action action : getActions())
    		{
    			action.act(delta);
    			this.removeAction(action);
    		}
    	else
    		for(Action action : getActions())
    		{
    			this.removeAction(action);
    		}
    }
    @Override
    public void draw(Batch spriteBatch, float delta)
    {
    	
    	if (isPlayer && canMove)
	    	switch (direction)
	    	{
	    	case 0:
	    		currentRegion = down;
	    		break;
	    	case 1:
	    		currentRegion = left;
	    		break;
	    	case 2:
	    		currentRegion = up;
	    		break;
	    	case 3:
	    		currentRegion = right;
	    		break;
	    	case 4:
	    		currentRegion = down_left;
	    		break;
	    	case 5:
	    		currentRegion = up_left;
	    		break;
	    	case 6:
	    		currentRegion = up_right;
	    		break;
	    	case 7:
	    		currentRegion = down_right;
	    		break;
			default:
				currentRegion = down;
				break;
	    	}
    	checkCurScreenLocation();

        if (atlas != null)
        	spriteBatch.draw(currentRegion, getX(), getY());
    }
    private void checkCurScreenLocation()
    {
    	int baseline = (int) (SCREEN_HEIGHT/4);
    	if (getX() < 0)
    		setX(0);
    	if (getX() > SCREEN_WIDTH-getWidth())
    		setX(SCREEN_WIDTH-getWidth());
    	if (getY() < baseline)
    		setY(baseline);
    	if (getY() > SCREEN_HEIGHT-getHeight())
    		setY(SCREEN_HEIGHT-getHeight());
    }

	public MyActor hit(float arg0, float arg1) {
		
		return null;
	}

	public void enableMovement() {
		this.canMove = true;
	}
	public void haltMovement()
	{
		this.canMove = false;
	}
}