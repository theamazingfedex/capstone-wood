/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActor;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.CameraController;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

public class StoreScreen implements Screen {

	Stage stage;
	RoombyGDX game;
	
	
	Texture texture;
	SpriteBatch batch;
//	OrthographicCamera camera;
//	GestureDetector gestureDetector;
	private Array<ObjectActor> objects;
	private CameraController controller;
	
	
	public StoreScreen(RoombyGDX game)
	{
		this.game = game;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
		Gdx.input.setInputProcessor(stage);
		objects = game.loadItemObjects();
		texture = new Texture("badlogic.jpg");
		batch = new SpriteBatch();
//		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//		controller = new CameraController(camera);
//		gestureDetector = new GestureDetector(0, 0.5f, 2, 0.15f, controller);
		Table scrollTable = new Table(Assets.getSkin());
		for(ObjectActor obj : objects)
		{
			scrollTable.add(obj);
			scrollTable.add(new Label(obj.getName(),RoombySkin.getLabelStyle(88))).row();
		}
		
		ScrollPane pane = new ScrollPane(scrollTable);
		pane.setStyle(RoombySkin.getScrollPaneStyle());
		pane.setWidth(Gdx.graphics.getWidth()*(2/3));
		pane.setHeight(Gdx.graphics.getHeight()*(2/3));
		pane.setX(Gdx.graphics.getWidth()/2);
		pane.setY(Gdx.graphics.getHeight()/2);
		pane.setVisible(true);
		 
		Table container = new Table();
		container.setFillParent(true);
		container.add(pane).fill().expand();
		
		stage.addActor(container);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.74f, 0.49f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		if(Gdx.input.isKeyPressed(Keys.BACK))
		{
			game.setScreen(game.dashboardScreen);
		}
		
//		controller.update();
//		camera.update();
//		batch.setProjectionMatrix(camera.combined);
//		batch.begin();
//		batch.draw(texture, 0, 0, texture.getWidth() * 2, texture.getHeight() * 2);
//		batch.end();
		
	}
	
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		texture.dispose();
		batch.dispose();
		stage.clear();
		stage.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}


}
