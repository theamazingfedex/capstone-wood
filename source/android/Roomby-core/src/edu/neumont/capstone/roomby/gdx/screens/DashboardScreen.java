package edu.neumont.capstone.roomby.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.google.android.gms.games.Player;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.actors.RoombyPlayer;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.MyColor;
import edu.neumont.capstone.roomby.gdx.tools.RoombySkin;

/**
 * 
 * @author Daniel Wood
 *
 */
public class DashboardScreen implements Screen {

//	public RoombyGDX game;
//	public Stage stage;
//	public SpriteBatch batch;
	
	private Stage stage = new Stage();
	private Table table = new Table();
	private Skin buttonSkin;
	private RoombyGDX game;
	private TextButton testzoneButton;
	private TextButton startGameButton;
	private TextButton logoutButton;
	private float buttonX = 75;
    private float currentY = 520f;
	private TextButton userOptionsButton;
	private TextButton storeButton;
	private TextButton buddyListButton;
	private Label usernameLabel;
	private BitmapFont font;
	private RoombyPlayer currentPlayer;
	private Player currentGooglePlayer;
	private FitViewport viewport;
	private OrthographicCamera camera;
	private int screenWidth;
	private int screenHeight;
	private float scaleMultiplier = 1;
	private float BUTTON_WIDTH = 300f;
	private float BUTTON_HEIGHT = 300f;
	private float BUTTON_SPACING = 30f;
	private int widthMultiplier;
	private int heightMultiplier;
	private boolean isLoaded;
	
	
	public DashboardScreen(RoombyGDX game){
		this.game = game;
		stage = new Stage();
//		this.gameHelper = game.androidApp.getGameHelper();
	}
	public DashboardScreen(RoombyGDX game, Stage stage){
		this.stage = stage;
		this.game = game;
		
//		this.gameHelper = game.androidApp.getGameHelper();
	}
	
	
	
	@Override
	public void render(float delta){
		Gdx.gl.glClearColor(MyColor.BLUE.getValue().r,
							MyColor.BLUE.getValue().g, 
							MyColor.BLUE.getValue().b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if (Assets.update())
			isLoaded = true;
		else
			isLoaded = false;
		if(Gdx.input.isKeyPressed(Keys.BACK))
		{
			if (game.resolver.isSignedIn())
			{
				game.resolver.saveUserData(game.currentPlayer);
				game.setScreen(game.logoutScreen);
			}
			else
				Gdx.app.exit();
		}
//		Log.d("Dashboard Render", "current player: " + currentPlayer.getActor().spritesheet);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}


	@Override
	public void resize(int width, int height)
	{

		widthMultiplier = width/1080;
		heightMultiplier = height/1920;
		BUTTON_HEIGHT *= heightMultiplier;
		BUTTON_WIDTH *= widthMultiplier;
		viewport.setScaling(Scaling.fit);
		viewport.update(width, height);
		stage.setViewport(viewport);
		stage.getCamera().update();
	}

	@Override
	public void show() {
		Assets.finishLoading();
		Gdx.input.setCatchBackKey(true);
		isLoaded = false;
		stage = new Stage();
		currentPlayer = game.currentPlayer;
		currentGooglePlayer = game.getSignedInPlayer();
		buttonSkin = RoombySkin.getSkin();
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		camera = (OrthographicCamera) stage.getCamera();
        camera.setToOrtho(false, screenWidth, screenHeight);
        
        viewport = new FitViewport(screenWidth, screenHeight, camera);
        
        stage.setViewport(viewport);

		TextButtonStyle tbs = buttonSkin.get("default", TextButtonStyle.class);
		
		startGameButton = new TextButton( "Start game", tbs );
		testzoneButton = new TextButton( "Test Zone", tbs );
		buddyListButton = new TextButton ( "Buddy List", tbs );
		storeButton = new TextButton("Item Browser", tbs);
		userOptionsButton = new TextButton("Options", tbs);
		logoutButton = new TextButton( "Log out", tbs );
		
        
        // button "start game"
//        startGameButton.setWidth(BUTTON_WIDTH);
//        startGameButton.setHeight(BUTTON_HEIGHT);
        startGameButton.addListener(new ClickListener(0){
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
//        		game.androidApp.log("Start-Clicked", "(" + String.valueOf(event.getRelatedActor().getX()) + ", " + String.valueOf(event.getRelatedActor().getY()) + ")");
        		if (isLoaded)
        		game.setScreen(game.gameScreen);
        		
        	}
        });
//        stage.addActor( startGameButton );
 
        // button "options"
//        optionsButton.setWidth(BUTTON_WIDTH);
//        optionsButton.setHeight(BUTTON_HEIGHT);
        testzoneButton.addCaptureListener(new ClickListener(0){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				if (isLoaded)
				game.setScreen(game.testzoneScreen);
			}
		});
//        stage.addActor( optionsButton );
 
//        logoutButton.setWidth(BUTTON_WIDTH);
//        logoutButton.setHeight(BUTTON_HEIGHT);
        
        logoutButton.addCaptureListener(new ClickListener(0){
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				if (isLoaded)
				game.setScreen(game.logoutScreen);
			}
		});

        // button "buddy list"
//        buddyListButton.setWidth(BUTTON_WIDTH);
//        buddyListButton.setHeight(BUTTON_HEIGHT);
        buddyListButton.addListener(new ClickListener(0){
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
        		if (isLoaded)
//        		game.androidApp.log("Start-Clicked", "(" + String.valueOf(event.getRelatedActor().getX()) + ", " + String.valueOf(event.getRelatedActor().getY()) + ")");
        		game.setScreen(game.buddyListScreen);
        	}
        });
        
        // button "Item Store"
//        storeButton.setWidth(BUTTON_WIDTH);
//        storeButton.setHeight(BUTTON_HEIGHT);
        storeButton.addListener(new ClickListener(0){
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
        		if (isLoaded)
//        		game.androidApp.log("Start-Clicked", "(" + String.valueOf(event.getRelatedActor().getX()) + ", " + String.valueOf(event.getRelatedActor().getY()) + ")");
        		game.setScreen(game.storeScreen);
        	}
        });
        
        // button "User Profile"
//        userProfileButton.setWidth(BUTTON_WIDTH);
//        userProfileButton.setHeight(BUTTON_HEIGHT);
        userOptionsButton.addListener(new ClickListener(0){
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
        		if (isLoaded)
//        		game.androidApp.log("Start-Clicked", "(" + String.valueOf(event.getRelatedActor().getX()) + ", " + String.valueOf(event.getRelatedActor().getY()) + ")");
        		game.setScreen(game.userOptionsScreen);
        	}
        });

        usernameLabel = new Label("Welcome,\n   "+currentGooglePlayer.getDisplayName()+"!", RoombySkin.getLabelStyle(88));
        usernameLabel.setScale(4);
        BUTTON_WIDTH *= screenWidth/1080;
        BUTTON_HEIGHT *= screenHeight/1920;
        table.add(usernameLabel).fill().colspan(2).padBottom(100).center().row();
        table.add(startGameButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT);
//		table.add(buddyListButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).row();
        table.add(userOptionsButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).row();
		table.add(storeButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT);
		table.add(logoutButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).row();
//		table.add(testzoneButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT).row();
		
//		table.setX(screenWidth/2-table.getWidth()/2);
//		table.setY(screenHeight/2);
		
		table.setFillParent(true);
		stage.addActor(table);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
//		game.resolver.onStop();
	}


	@Override
	public void dispose() {
//		if (buttonSkin != null)
//			buttonSkin.dispose();
		if (table != null)
			table.clear();
		if (stage != null)
			stage.dispose();
	}
	@Override
	public void resume() {
//		stage = new Stage();
//		game.resolver.onStart();
	}

}
