/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.actors;

import java.io.Serializable;

import edu.neumont.capstone.roomby.gdx.tools.ObjectTypes;


public class ObjectActorConfig implements Serializable{

	private String imageName;
	private float x;
	private float y;
	private float width, height;
	private ObjectTypes objectType;
	private boolean touchable;
	private int imageScale;
	private String writeField;
	

	

	public ObjectActorConfig(String imageName, float xPos, float yPos, ObjectTypes type, boolean touchable, int imageScale)
	{
		this.imageName = imageName;
		this.x = xPos;
		this.y = yPos;
		this.objectType = type;
		this.setTouchable(touchable);
		this.setImageScale(imageScale);
		this.writeField = "Share something here!";
	}

	public ObjectActorConfig(){};

	public void setActorName(String name)
	{
		this.writeField = name;
	}
	public String getActorName()
	{
		return this.writeField;
	}
	
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
		this.writeField = imageName;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public ObjectTypes getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectTypes objectType) {
		this.objectType = objectType;
	}

	public boolean isTouchable() {
		return touchable;
	}

	public void setTouchable(boolean touchable) {
		this.touchable = touchable;
	}

	public int getImageScale() {
		return imageScale;
	}

	public void setImageScale(int imageScale) {
		this.imageScale = imageScale;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}
	public String getWriteField() {
		return writeField;
	}

	public void setWriteField(String writeField) {
		
		this.writeField = writeField;
	}
}
