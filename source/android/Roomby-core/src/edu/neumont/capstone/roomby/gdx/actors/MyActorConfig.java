package edu.neumont.capstone.roomby.gdx.actors;

import java.io.Serializable;

import edu.neumont.capstone.roomby.gdx.tools.PlayerSprites;

public class MyActorConfig implements Serializable{

	public PlayerSprites spritesheet;
	public float x;
	public float y;
	
	public MyActorConfig(){}
	public MyActorConfig(PlayerSprites spritesheet, float x, float y)
	{
		this.spritesheet = spritesheet;
		this.x = x;
		this.y = y;
	}
}
