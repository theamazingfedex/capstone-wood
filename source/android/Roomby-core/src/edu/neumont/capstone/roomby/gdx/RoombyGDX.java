package edu.neumont.capstone.roomby.gdx;

import java.util.ArrayList;

import android.content.Context;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.Array;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Player;

import edu.neumont.capstone.roomby.gdx.actors.MyActorConfig;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActor;
import edu.neumont.capstone.roomby.gdx.actors.ObjectActorConfig;
import edu.neumont.capstone.roomby.gdx.actors.RoombyPlayer;
import edu.neumont.capstone.roomby.gdx.screens.BuddyListScreen;
import edu.neumont.capstone.roomby.gdx.screens.DashboardScreen;
import edu.neumont.capstone.roomby.gdx.screens.GameScreen;
import edu.neumont.capstone.roomby.gdx.screens.LogoutScreen;
import edu.neumont.capstone.roomby.gdx.screens.TestzoneScreen;
import edu.neumont.capstone.roomby.gdx.screens.SplashScreen;
import edu.neumont.capstone.roomby.gdx.screens.StoreScreen;
import edu.neumont.capstone.roomby.gdx.screens.UserOptionsScreen;
import edu.neumont.capstone.roomby.gdx.tools.AndroidActionResolver;
import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.MyColor;
import edu.neumont.capstone.roomby.gdx.tools.PlayerSprites;


/**
 * 
 * @author Daniel Wood
 *
 */
public class RoombyGDX extends Game implements ApplicationListener{ //implements InputProcessor{
//	public static final String LOG = "Rmby";
	public DashboardScreen dashboardScreen;
	public LogoutScreen logoutScreen;
	public GameScreen gameScreen;
	public AndroidApplication androidApp;
	public SplashScreen splashScreen;
	public TestzoneScreen testzoneScreen;
	public BuddyListScreen buddyListScreen;
	public StoreScreen storeScreen;
	public UserOptionsScreen userOptionsScreen;
	public RoombyPlayer currentPlayer;
	
	private Player player;
	private String playerRoomId;
	
	float w,h;
	private GoogleApiClient apiClient;
	public AndroidActionResolver resolver;
	private Context appContext;
	
	class TouchInfo{
		public float xPos = 0;
		public float yPos = 0;
		public boolean isTouched = false;
	}
	public RoombyGDX(final AndroidApplication androidApp, AndroidActionResolver resolver) {
		// TODO Auto-generated constructor stub
		this.androidApp = androidApp;
		appContext = androidApp.getApplicationContext();
		resolver.setAppContext(appContext);
		this.resolver = resolver;
	}

	
	public void log(String tag, String message)
	{
		androidApp.log(tag, message);
	}
	
	public RoombyPlayer setNewPlayer(MyActorConfig actor)
	{
		this.currentPlayer = new RoombyPlayer(actor);
		resolver.saveUserData(currentPlayer);
		return currentPlayer;
	}
	public RoombyPlayer getCurPlayer()
	{
		if (currentPlayer == null)
			currentPlayer = resolver.loadUserData();
		return this.currentPlayer;
	}
	public void updatePlayerActor(MyActorConfig actor)
	{
//		currentPlayer = resolver.loadUserData();
		currentPlayer.setActorConfig(actor);
		resolver.saveUserData(currentPlayer);
	}
	@Override
	public void create () {
		
		dashboardScreen = new DashboardScreen(this);
		logoutScreen = new LogoutScreen(this);
		gameScreen = new GameScreen(this);
		splashScreen = new SplashScreen(this);
		testzoneScreen = new TestzoneScreen(this);
		buddyListScreen = new BuddyListScreen(this);
		storeScreen = new StoreScreen(this);
		userOptionsScreen = new UserOptionsScreen(this);
		
//		player = resolver.getSignedInPlayer();
//		playerRoomId = player.getPlayerId();
		
		Gdx.gl.glClearColor(0, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		this.setScreen(splashScreen);
//		while (!resolver.isSignedIn()){
//		}
//		currentPlayer = resolver.loadUserData();
//		if (currentPlayer == null)// || currentPlayer.getActor() == null)
//		{	
//			setNewPlayer(new MyActorConfig(PlayerSprites.BROWN_BROWN, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2));
//		}
//		if (currentPlayer.getActor() == null)
//		{
//			currentPlayer.setActorConfig(new MyActorConfig(PlayerSprites.BROWN_BROWN, Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2));
//		}
		

//		initializePlayer(player);
		Assets.queueLoading();
	}

	@Override
	public void render () {
		super.render();
//		getScreen().render(Gdx.graphics.getDeltaTime());
//		Gdx.gl.glClearColor(0, 1, 1, 1);
//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Assets.update();

	}
	
	@Override
	public void resume(){
//		resolver.onStart();
//		currentPlayer = resolver.loadUserData();
		Assets.queueLoading();
	}
	
	@Override
	public void pause(){
//		resolver.saveUserData(getCurPlayer());
//		resolver.onStop();
//		Assets.disposeAll();
	}

	public boolean isSignedIn(){
	
		return resolver.isSignedIn();
	}
	public void showAchievements()
	{
		resolver.showAchievements();
	}
	public void showLeaderboard()
	{
		resolver.showLeaderBoard();
	}
	public Player getSignedInPlayer()
	{
		return resolver.getSignedInPlayer();
	}
	public void logout() {
		resolver.signOut();
		Assets.resetManager();
		if (!resolver.isSignedIn())
			Gdx.app.exit();
		
//		setScreen(splashScreen);
	}

	public void setClient(GoogleApiClient apiClient) {
		this.apiClient = apiClient;
		
	}
	public GoogleApiClient getApiClient(){
		return this.apiClient;
	}

	public void setPlayer(Player signedInPlayer) {
		this.player = signedInPlayer;
		
	}
	public Player getPlayer(){
		player = resolver.getSignedInPlayer();
		setPlayerRoomId(player.getPlayerId());
		return this.player;
	}


	public String getPlayerRoomId() {
		playerRoomId = resolver.getSignedInPlayer().getPlayerId();
		return playerRoomId;
	}


	public void setPlayerRoomId(String playerRoomId) {
		this.playerRoomId = playerRoomId;
	}




	/**
	 * @return an ArrayList of the ObjectActors defined by the
	 * 			loaded ObjectActorConfigs
	 */
	public Array<ObjectActor> loadItemObjects() 
	{
		ArrayList<ObjectActorConfig> configs = resolver.loadAllItems();
		Array<ObjectActor> items = new Array<ObjectActor>();
		
		for (int i = 0; i < configs.size(); i++)
		{
			items.add(new ObjectActor(configs.get(i)));
		}
		
		return items;
	}


	/**
	 * @param worldObjects
	 */
	public void updatePlayerItems(Array<ObjectActor> worldObjects) {
		ArrayList<ObjectActorConfig> configs = new ArrayList<ObjectActorConfig>();
		for (ObjectActor obj : worldObjects)
		{
			obj.updatePosition(obj.getX(), obj.getY());
			configs.add(obj.getConfig());
		}
//		resolver.savePlayerItems(configs);
		currentPlayer.setObjects(configs);
		resolver.saveUserData(currentPlayer);
		
	}


	/**
	 * @return Array<ObjectActor> loaded from userdata
	 */
	public Array<ObjectActor> loadPlayerItems() {
		if (currentPlayer == null)
			currentPlayer = resolver.loadUserData();
		ArrayList<ObjectActorConfig> configs = currentPlayer.getObjects();;
		if (configs == null)
			configs = new ArrayList<ObjectActorConfig>();
		Array<ObjectActor> items = new Array<ObjectActor>();
		for (ObjectActorConfig objConfig : configs)
			items.add(new ObjectActor(objConfig));
		return items;
	}


	/**
	 * @param curBgColor
	 * @param curCpColor
	 */
	public void updatePreferences(MyColor curBgColor, MyColor curCpColor, boolean isRightHanded) {
		if (currentPlayer == null)
			currentPlayer = resolver.loadUserData();
		currentPlayer.setControlPadBackgroundColor(curCpColor);
		currentPlayer.setGameScreenBackgroundColor(curBgColor);
		currentPlayer.setRightHanded(isRightHanded);
		resolver.saveUserData(currentPlayer);
		
	}
}