package edu.neumont.capstone.roomby.gdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;

public class BuddyListScreen implements Screen{

	Stage stage;
	RoombyGDX game;
	
	
	public BuddyListScreen(RoombyGDX game)
	{
		this.game = game;
		this.stage = new Stage();
	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.49f, 0.74f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		if(Gdx.input.isKeyPressed(Keys.BACK))
			game.setScreen(game.dashboardScreen);
			
		if(Gdx.input.justTouched())
		{
			game.showAchievements();
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		//scale by multiplying the difference of width/height times my S4 resolution
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);
	}

}
