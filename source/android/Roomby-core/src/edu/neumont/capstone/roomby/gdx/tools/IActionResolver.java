/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.tools;

public interface IActionResolver {
    public boolean isGPGSAvailable();
    public void showLeaderBoard();
    public void showAchievements();
    public void showSettings();

    public void submitScore(long score);
    public void unlockAchievement(Achievements achievement);
    public void signIn();
    public void signOut();
    public boolean isSignedIn();

    public void registerActionListener(IActionListener listener);
    public void unRegisterActionListener(IActionListener listener);

    public void sendScreenView(String screen);
}