/**
 * @Author: Daniel Wood
 */
package edu.neumont.capstone.roomby.gdx.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import edu.neumont.capstone.roomby.gdx.tools.Assets;
import edu.neumont.capstone.roomby.gdx.tools.ObjectTypes;


public class ObjectActor extends Actor {

	int SCREEN_WIDTH = Gdx.graphics.getWidth();
	int SCREEN_HEIGHT = Gdx.graphics.getHeight();
	
	private Texture objTexture = null;
	private ObjectActorConfig config;
	private ObjectTypes objectType;
	private boolean dragging = false;
	private int imageScale;
	private String writeField;
	

	public ObjectActor(ObjectActorConfig objc) {
		this.setConfig(objc);
		this.setName(config.getImageName().split("\\.")[0]);
		this.setTouchable(Touchable.enabled);
		this.setX(config.getX());
		this.setY(config.getY());
		this.setImageScale(config.getImageScale());
		this.setObjectType(config.getObjectType());
		objTexture = Assets.get(config.getImageName(), Texture.class);
		this.setWidth(objTexture.getWidth()*imageScale);
		this.setHeight(objTexture.getHeight()*imageScale);
		
	}
//	@Override
//	public void setName(String name)
//	{
//		config.setActorName(name);
//		super.setName(name);
//	}
	@Override
	public void draw(Batch batch, float delta)
	{
//		batch.end();
//		batch.begin();
		
		if (dragging)
			checkCurScreenLocation();
		batch.draw(objTexture, getX(), getY(), getWidth(), getHeight());
	}
	private void checkCurScreenLocation()
	{
    	int baseline = SCREEN_HEIGHT/4;
    	if (getX() < 0)
    		setX(0);
    	if (getX() > SCREEN_WIDTH-getWidth())
    		setX(SCREEN_WIDTH-getWidth());
    	if (getY() < baseline)
    		setY(baseline);
    	if (getY() > SCREEN_HEIGHT-getHeight())
    		setY(SCREEN_HEIGHT-getHeight());
    }
	
	
	public void setObjectType(ObjectTypes objectType) 
	{
		this.objectType = objectType;
	}
	public ObjectTypes getObjectType()
	{
		return this.objectType;
	}
	public ObjectActorConfig getConfig() {
		return config;
	}
	public void setConfig(ObjectActorConfig config) {
		this.config = config;
	}

	public int getImageScale() {
		return imageScale;
	}

	public void setImageScale(int imageScale) {
		this.imageScale = imageScale;
	}

	/**
	 * @param x
	 * @param y
	 */
	public void updatePosition(float x, float y) 
	{
		setPosition(x, y);
		config.setX(x);
		config.setY(y);
	}
	public String getWriteField() {
		if (writeField == null)
			writeField = "Share something here!";
		return writeField;
	}
	public void setWriteField(String swriteField) {
		this.writeField = swriteField;
		config.setWriteField(swriteField);
	}
}
