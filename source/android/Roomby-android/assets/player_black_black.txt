player_black_black.png
format: RGBA8888
filter: Linear,Linear
repeat: none
down
  rotate: false
  xy: 2, 2
  size: 70, 127
  orig: 70, 127
  offset: 0, 0
  index: -1
down_left
  rotate: false
  xy: 74, 2
  size: 63, 127
  orig: 63, 127
  offset: 0, 0
  index: -1
down_right
  rotate: false
  xy: 139, 2
  size: 62, 127
  orig: 62, 127
  offset: 0, 0
  index: -1
left
  rotate: false
  xy: 203, 2
  size: 63, 127
  orig: 63, 127
  offset: 0, 0
  index: -1
right
  rotate: false
  xy: 2, 131
  size: 63, 127
  orig: 63, 127
  offset: 0, 0
  index: -1
up
  rotate: false
  xy: 67, 131
  size: 72, 128
  orig: 72, 128
  offset: 0, 0
  index: -1
up_left
  rotate: false
  xy: 141, 131
  size: 60, 127
  orig: 60, 127
  offset: 0, 0
  index: -1
up_right
  rotate: false
  xy: 203, 131
  size: 60, 127
  orig: 60, 127
  offset: 0, 0
  index: -1
