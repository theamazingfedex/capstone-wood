	package edu.neumont.capstone.roomby.gdx.android;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;

import edu.neumont.capstone.roomby.gdx.RoombyGDX;
import edu.neumont.capstone.roomby.gdx.tools.AndroidActionResolver;

/**
 * 
 * @author Daniel Wood
 *
 */ 

public class AndroidLauncher extends AndroidApplication implements ConnectionCallbacks// implements GameHelperListener //, ConnectionCallbacks, OnConnectionFailedListener 
{

	  
    private String LB_ID;
    private RoombyGDX game;
    private AndroidApplicationConfiguration config;
	private AndroidActionResolver resolver;
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		mSignInClicked = true;
//		gameHelper = new GameHelper(this, GameHelper.CLIENT_ALL);
//		gameHelper.enableDebugLog(true);
//		gameHelper.setup(this);
		setLogLevel(LOG_DEBUG);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
//		LB_ID = getResources().getString(R.string.leaderboard_id);

//		apiClient = setupApiClient(gameHelper);
		config = new AndroidApplicationConfiguration();
		config.useAccelerometer = false;
//		if (person == null){
//			loginGPGS();
//		} else {
//		}
		resolver = new AndroidActionResolver(this, this);
		game = new RoombyGDX(this, resolver);//, Plus.PeopleApi.getCurrentPerson(apiClient));
		initialize(game, config);
		
//		resolver.signIn();
		
		game.setClient(resolver.getApiClient());
//		game.setPlayer(resolver.getSignedInPlayer());
		
//		game.initializePlayer(resolver.getSignedInPlayer());
		
		
//			setContentView(new View(getApplicationContext()));
	}
	@Override
	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		super.onActivityResult(requestCode, responseCode, intent);
		resolver.onActivityResult(requestCode, responseCode, intent);
		
//		onSignInSucceeded();
	}
	
	@Override
	protected void onStart() {
	    super.onStart();
	    resolver.onStart();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	    
//	    resolver.onStop();
	}

//	@Override
//	protected void onResume(){
//		super.onResume();
//	}
	
//	@Override
//	public boolean getSignedInGPGS() {
//		return gameHelper.isSignedIn();
//	}
//
//	@Override
//	public void loginGPGS() {
//		try {
//			runOnUiThread(new Runnable(){
//				public void run() {
//					gameHelper.beginUserInitiatedSignIn();
//				}
//			});
//		} catch (final Exception ex) {
//		}
//	}
//
//	@Override
//	public void submitScoreGPGS(int score) {
//		Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), LB_ID);
//		Games.Leaderboards.submitScore(gameHelper.getApiClient(), LB_ID, score);
//	}
//
//	@Override
//	public void unlockAchievementGPGS(String achievementId) {
//		Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
//	}
//
//	@Override
//	public void getLeaderboardGPGS() {
//		startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(),
//		        LB_ID), 0);
//		
//	}
//
//	@Override
//	public void getAchievementsGPGS() {
//		startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 0);
//	}


	
	public void makeToast(String message, boolean isShownLong)
	{
		if (isShownLong)
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		else
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
/* (non-Javadoc)
 * @see com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks#onConnected(android.os.Bundle)
 */
@Override
public void onConnected(Bundle arg0) {
	Gdx.app.getPreferences("AUTO_SIGN_IN").putBoolean("finishedShowing", true);
	Gdx.app.getPreferences("AUTO_SIGN_IN").flush();
	
}
/* (non-Javadoc)
 * @see com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks#onDisconnected()
 */
@Override
public void onDisconnected() {
	// TODO Auto-generated method stub
	
}
}
